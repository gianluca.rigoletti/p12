<?php

  include "Librerie/start_db.php";

  include "Librerie/ges_db.php";

?>

<!DOCTYPE HTML>

<head>


<!-------- APP -------->



<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0 minimal-ui"/>

<meta name="apple-mobile-web-app-capable" content="yes"/>

<meta name="apple-mobile-web-app-status-bar-style" content="black">



<!-------- FINE APP -------->



<!-------- ICON APP -------->



<link rel="apple-touch-icon" sizes="196x196" href="immagini/icone/apple-touch-icon-196x196.png">

<link rel="apple-touch-icon" sizes="180x180" href="immagini/icone/apple-touch-icon-180x180.png">

<link rel="apple-touch-icon" sizes="152x152" href="immagini/icone/apple-touch-icon-152x152.png">

<link rel="apple-touch-icon" sizes="144x144" href="immagini/icone/apple-touch-icon-144x144.png">



<!-------- FINE ICON APP -------->



<title><?php if (isset($title)) echo $title;?></title>





<link href="styles/style.css" rel="stylesheet" type="text/css">

<link href="styles/framework.css" rel="stylesheet" type="text/css">

<link href="styles/font-awesome.css" rel="stylesheet" type="text/css">

<link href="styles/animate.css" rel="stylesheet" type="text/css">



<script type="text/javascript" src="scripts/jquery.js"></script>

<script type="text/javascript" src="scripts/jqueryui.js"></script>

<script type="text/javascript" src="scripts/framework-plugins.js"></script>

<script type="text/javascript" src="scripts/custom.js"></script>

<script type="text/javascript" src="scripts/jquery.validate.min.js"></script>



</script>



<!--- Valutazione --->

<link href="styles/rateit.css" rel="stylesheet" type="text/css">

<!--- Fine Valutazione --->



</head>



<body <?php if (isset($classeBody)) echo "class=\"$classeBody\"";?> >



<img class="landscape" src="immagini/schermo.jpg" />



<div id="preloader">

	<div id="status">

        <div class="preloader-logo"></div>

        <p class="center-text smaller-text">

           Pagina in caricamento...

        </p>

    </div>

</div>


<?php if (isset($no_header) && $no_header == false): ?>



  <div id="header-fixed">

  <ul>

  <li><a href="#">PERCORSO</a>

  <ul class="submenu">

  <li><a href="1sette-giugno.php"> 17 giugno 2016</a></li>

  <li><a href="1otto-giugno.php"> 18 giugno 2016</a></li>

  </ul>

  </li>

  <li><a href="relatori.php">RELATORI</a></li>

  <li><a href="#">SPONSOR</a>

  <ul class="submenu">

  <li><a href="collaborazione.php">relatori</a></li>

  <li><a href="supporto.php">tavola rotonda</a></li>

  <li><a href="ringrazia.php">altri</a></li>

  </ul>

  </li>

  <li><a href="rassegna-stampa.php">PRIME PAGINE</a></li>

  <li><a href="anteo.php">ANTEO</a></li>

  <li><a href="questionario.php">QUESTIONARIO</a></li>

  <li><a href="index.php?logout=y">LOGOUT</a></li>

   <li style=" padding-left:10px;"><a class="indietro" href="javascript:history.back()" style="padding:20px 10px 20px 0px"><img src="immagini/back.png" width="20px" /></a></li>
   <li><a class="indietro" href="javascript:window.history.forward()" style="padding:20px 0px 20px 10px"><img src="immagini/next.png" width="20px" /></a></li>

  </ul>

  </div>


<?php endif; ?>



<?php

  if (isset($_POST['user']) && isset($_POST['password']) ) {

    if (empty($_POST['user']) || empty($_POST['password'])) {

      $_GET['error'] = true;

    } else {

      $sql   = " SELECT * FROM user WHERE utente=".$_POST['user']." AND password = ".$_POST['password'];

      $query = mysql_query($sql);

      $rows  = mysql_num_rows($query);

      if ($rows > 0 ) {

        $res = mysql_fetch_assoc($query);

        $_SESSION['id_utente'] = $res['ID'];

        $_SESSION['user']  = $res['utente'];

        $_SESSION['nome']  = $res['nome'];

        $_SESSION['login'] = true;


        echo "

          <script> window.location.href = 'index-loggato.php'; </script>

        ";

      } else {

        $_GET['error'] = true;

      }

    }

  }



  if (isset($_GET['logout']) && $_GET['logout'] == 'y') {

    unset($_SESSION['user']);

    unset($_SESSION['nome']);

    unset($_SESSION['id_utente']);

    $_SESSION['login'] = false;

  }



  if ( (!isset($_SESSION['login']) || !$_SESSION['login']) && basename($_SERVER['SCRIPT_FILENAME']) != 'index.php' &&
        basename($_SERVER['SCRIPT_FILENAME']) != 'percentuali.php'


     ) {

    echo "

      <script> window.location.href = 'index.php'; </script>

    ";

  }

?>
