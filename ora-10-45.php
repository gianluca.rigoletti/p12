<?php
  $title = "Ore 10.45";
  $no_header = false;
  include "header.php";
?>

<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="two-third-responsive blue">
                <h1>programma 17 giugno</h1>
                </div>

                <div class="one-third-responsive last-column red2">
                <h1>ore 10.45</h1>
                </div>


                <div class="clear"></div>

                <div class="two-third-responsive supervisor">
                <h1>Dall’approccio multisettoriale alla gestione dinamica del rischio nei portafogli reddito fisso</h1>
                <em>Nicola Meotti, Director - Financial Institutions - AB</em>

                <p>Commenti del relatore. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem.</p>

                </div>


                <div class="one-third-responsive last-column">
                <img class="responsive-image half-bottom" src="immagini/relatori/prova.jpg" />
                <img class="logo2" src="immagini/relatori/loghi/prova.jpg" />

                </div>

               <div class="one-third-responsive b">
               <a href="presentazione.php">
                            <img class="responsive-image half-bottom" src="immagini/relatori/icone/presentazione.jpg" />
                            <p class="center-text">
                              presentazione
                            </p>
                </a>
                        </div>
                        <div class="one-third-responsive b">
               <a href="valutazione.php?id=17_10-45">
                            <img class="responsive-image half-bottom" src="immagini/relatori/icone/gradimento.jpg" />
                            <p class="center-text">
                               gradimento
                            </p>
               </a>
                        </div>
                        <div class="one-third-responsive last-column b">
              <a href="speaker-10-45.php">
                            <img class="responsive-image half-bottom" src="immagini/relatori/icone/speaker.jpg" />
                            <p class="center-text">
                                CV
                            </p>
                </a>
                        </div>


                <div class="clear"></div>




            </div>

         <?php include "footer.php" ?>


        </div>



</body>
