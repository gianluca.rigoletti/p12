<?php
  $title = "gradimento";
  $no_header = false;
  include "header.php";

  $sessioni = array(
                "17_10-45" => array(
                  "oratore" => "Nicola Meotti, Director - Financial Institutions - AB",
                  "from"    => "ora-10-45.php"
                ),
                "18_xx-xx" => array(
                  "oratore" => "Nicola Meotti, Director - Financial Institutions - AB",
                  "from"    => "ora-10-45.php"
                ),
                "18_yy-yy" => array(
                  "oratore" => "Nicola Meotti, Director - Financial Institutions - AB",
                  "from"    => "ora-10-45.php"
                )
              );
  $update = false;

  // Controllo se sono in inserimento o in update
  if (db_query_count('gradimento', ' id_utente = '.$_SESSION['id_utente'].' AND codice_sessione = "'.$_GET['id'].'"') > 0) {
    $update = true;
  }

  //  Controllo se è stata inserita una valutazione
  if (isset($_POST['inviaValutazione'])) {
    $nome_oratore     = (db_is_null($sessioni[$_POST['codice_sessione']]['oratore'])) ? null : "'".$sessioni[$_POST['codice_sessione']]['oratore']."'";
    $valutazione      = (db_is_null($_POST['valutazione'])) ? null : $_POST['valutazione'];
    $commento         = (db_is_null($_POST['commento'])) ? null : "'".$_POST['commento']."'" ;
    // Controllo se aggiornare o inserire il record per la prima volta
    if ($update) {
      $sql = " UPDATE gradimento SET
             valutazione = ".$valutazione.",
             commento = ".$commento."
             WHERE id_utente = ".$_SESSION['id_utente']." AND codice_sessione = '".$_GET['id']."'";
    } else {
      $sql = " INSERT INTO gradimento (id_utente, codice_sessione, valutazione, commento, nome_relatore)
               VALUES ( ".$_SESSION['id_utente'].", '".$_POST['codice_sessione']."', ".$valutazione.", '".$commento."', ".$nome_oratore." )";
    }
    if (!mysql_query($sql)) {
      die(mysql_error().$sql);
      // come gestire il fallimento della query?
    }
    // Redirect sulla pagina dell'intervento
    $string = "<script> window.location.href = '".$sessioni[$_POST['codice_sessione']]['from']."'; </script>";
    echo $string;
    //die(var_dump($string));
  }

  // Controllo se è gia presente una valutazione nel db
  $voti     = db_query_generale('gradimento', ' id_utente = '.$_SESSION['id_utente'].' AND codice_sessione = "'.$_GET['id'].'"', 'id_utente');
  $res_voti = mysql_fetch_assoc($voti);

?>



<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="two-third-responsive blue">
                <h1>gradimento</h1>
                </div>

                <div class="one-third-responsive last-column red2">
                </div>


                <div class="clear"></div>

                <form class="" action="valutazione.php?id=<?php echo $_GET['id'] ?>" method="post">
                  <input type="hidden" name="codice_sessione" value="<?php echo $_GET['id'] ?>">
                  <input type="hidden" name="valutazione" id="valutazione" value="<?php if (isset($res_voti['valutazione'])) echo $res_voti['valutazione']; ?>">
                  <div class="two-third-responsive supervisor">

                    <h1>Intervento</h1>
                    <em><?php echo $sessioni[$_GET['id']]['oratore']; ?></em>

                    <div class="rateit" id="rateit5" data-rateit-min="0" data-rateit-value="<?php if (isset($res_voti['valutazione'])) echo $res_voti['valutazione']; ?>"></div>
                    <div>
                    <span id="value"></span><span id="hover5"></span>
                    </div>

                    <script type="text/javascript">
            			    $(function () { $('#rateit5').rateit({ max: 10, step: 1 }); });
                            $("#rateit5").bind('rated', function (event, value) {
                              $('#value').text('Hai votato: ' + value);
                              $('#valutazione').val(value);
                            });
                            $("#rateit5").bind('reset', function () { $('#value').text('Valutazione Cancellata'); });
                    </script>
                    <script src="scripts/jquery.rateit.js" type="text/javascript"></script>

                     <p> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>

                     <textarea class="comment" placeholder="LASCIA UN COMMENTO" name="commento"><?php if (isset($res_voti['commento'])) echo $res_voti['commento']; ?></textarea>

                  </div>

                  <div class="one-third-responsive last-column">
                  <button class="button button-red button-round" name="inviaValutazione">Invia</button>
                  </div>
                </form>








                <div class="clear"></div>



                </div>



               <?php include "footer.php" ?>

        </div>

</body>
