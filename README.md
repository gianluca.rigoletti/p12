## Note

* Non so se la percentuale delle categorie va calcolata rispetto al subtotale
della categoria o rispetto al totale delle risposte. In caso bastera aggiungere
una riga nella `$sql_totale` di `calcolaPercentualeCategoria`

* Non ho capito se andavano aggiunte le cifre vere e proprie delle percentuali
di fianco ai grafici. Non c'è nessun elemento html predisposto quindi ho supposto
di no
