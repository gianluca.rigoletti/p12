<?php
  $title = "questionario";
  $no_header = false;
  include "header.php";

  function implodeValori( $valori ) {
    // Ho usato un array filter per evitare di implodere valori vuoti
    return implode(";", array_filter($valori));
  }

  function implodeDescrizioni( $domanda, $descrizioni ) {
    //intersect serve a prendere un subset di un array
    return implode( ";", array_intersect_key( $domanda, array_flip( $descrizioni ) ) );
  }
  
  function setApici ($elem) {
     return preg_replace("/\\\'/","''",addslashes($elem));  
  } 

  /*
    Se dei valori non sono impostati li mette uguali a null
  */
  function setNull ( $value ) {
    if (!isset($value) || $value='' || $value=' ') {
      return null;
    }
    return $value;
  }

  // Array delle domande con campi memorizzati
  $domande = array(
    'domanda1' => array(
      1 => 'Fondazione bancaria',
      2 => 'Cassa di previdenza',
      3 => 'Fondo pensione',
      4 => 'Cassa o fondo sanitario',
      5 => 'Intermediario finanziario',
      6 => 'Prometeia'
    ),
    'domanda4' => array(
      1  => 'Lisbona 2005',
      2  => 'Atene 2006',
      3  => 'Praga 2007',
      4  => 'Santander 2008',
      5  => 'Berlino 2009',
      6  => 'Antalya 2010',
      7  => 'Amsterdam 2011',
      8  => 'Budapest 2012',
      9  => 'Amburgo 2013',
      10 =>'Parigi 2014',
      11 => 'Barcellona 2015'
    ),
    'domanda5' => array(
      1 => 'Il tema del convegno',
      2 => 'La possibilità di partecipare alle sessioni parallele',
      3 => 'Poter discutere temi d’investimento con i rappresentanti dell’industria',
      4 => 'Poter discutere temi d’investimento con gli investitori istituzionali',
      5 => 'Per attività di formazione professionale',
      6 => 'Per condividere problematiche e criticità di mercato',
      7 => 'La destinazione'
    ),
    'domanda7' => array(
      1 => 'xxx – Fondazione xxx',
      2 => 'xxx – Cassa xxx',
      3 => 'xxx – Fondo Pensione xxx',
      4 => 'xxx – xxx',
      5 => 'xxx – xxx',
      6 => 'xxx – xxx',
      7 => 'xxx – xxx'
    ),
    'domanda14' => array(
      1 => 'xxx – Fondazione xxx',
      2 => 'xxx – Cassa xxx',
      3 => 'xxx – Fondo Pensione xxx',
      4 => 'xxx – xxx',
      5 => 'xxx – xxx',
      6 => 'xxx – xxx',
      7 => 'xxx – xxx'
    )
  );

  //Variabili sulle domande
  $domanda1           = (isset($_POST['domanda1'])) ? $_POST['domanda1'] : array() ;
  $domanda1_altro     = (isset($_POST['domanda1_altro'])) ? setApici($_POST['domanda1_altro']) : '' ;
  $domanda2           = (isset($_POST['domanda2'])) ? setApici($_POST['domanda2']) : '' ;
  $domanda3           = (isset($_POST['domanda3'])) ? setApici($_POST['domanda3']) : '' ;
  $domanda4           = (isset($_POST['domanda4'])) ? $_POST['domanda4'] : array() ;
  $domanda5           = (isset($_POST['domanda5'])) ? $_POST['domanda5'] : array() ;
  $domanda6           = (isset($_POST['domanda6'])) ? setApici($_POST['domanda6']) : '' ;
  $domanda7           = (isset($_POST['domanda7'])) ? $_POST['domanda7'] : array() ;
  $domanda8           = (isset($_POST['domanda8'])) ? setApici($_POST['domanda8']) : '' ;
  $domanda9           = (isset($_POST['domanda9'])) ? setApici($_POST['domanda9']) : '' ;
  $domanda10          = (isset($_POST['domanda10'])) ? setApici($_POST['domanda10']) : '' ;
  $domanda11          = (isset($_POST['domanda11'])) ? setApici($_POST['domanda11']) : '' ;
  $domanda11_commento = (isset($_POST['domanda11_commento'])) ? setApici($_POST['domanda11_commento']) : '' ;
  $domanda12          = (isset($_POST['domanda12'])) ? setApici($_POST['domanda12']) : '' ;
  $domanda13          = (isset($_POST['domanda13'])) ? setApici($_POST['domanda13']) : '' ;
  $domanda14          = (isset($_POST['domanda14'])) ? $_POST['domanda14'] : array() ;
  $domanda15          = (isset($_POST['domanda15'])) ? setApici($_POST['domanda15']) : '' ;
  $domanda16          = (isset($_POST['domanda16'])) ? setApici($_POST['domanda16']) : '' ;
  $domanda17          = (isset($_POST['domanda17'])) ? setApici($_POST['domanda17']) : '' ;
  $domanda18          = (isset($_POST['domanda18'])) ? setApici($_POST['domanda18']) : '' ;
  $domanda19          = (isset($_POST['domanda19'])) ? setApici($_POST['domanda19']) : '' ;
  $domanda20          = (isset($_POST['domanda20'])) ? setApici($_POST['domanda20']) : '' ;
  $domanda21          = (isset($_POST['domanda21'])) ? setApici($_POST['domanda21']) : '' ;
  $domanda22          = (isset($_POST['domanda22'])) ? setApici($_POST['domanda22']) : '' ;
  $domanda23          = (isset($_POST['domanda23'])) ? setApici($_POST['domanda23']) : '' ;
  $domanda24          = (isset($_POST['domanda24'])) ? setApici($_POST['domanda24']) : '' ;
  $domanda25          = (isset($_POST['domanda25'])) ? setApici($_POST['domanda25']) : '' ;
  $domanda26          = (isset($_POST['domanda26'])) ? setApici($_POST['domanda26']) : '' ;
  $domanda27          = (isset($_POST['domanda27'])) ? setApici($_POST['domanda27']) : '' ;

  $update = false;
  //Controllo se sono in update o inserimento e decido la query
  if (db_query_count('questionario', 'id_utente = '.$_SESSION['id_utente']) > 0) {
    $update = true;
  }

  if (isset($_POST['inviaQuestionario'])) {
    if ($update) {
      $sql = " UPDATE questionario SET
        valori1            = '".implodeValori($domanda1)."',
        descrizione1       = '".implodeDescrizioni($domande['domanda1'], $domanda1)."',
        domanda1_altro     = '".$domanda1_altro."',
        domanda2           = '".$domanda2."',
        domanda3           = '".$domanda3."',
        valori4            = '".implodeValori($domanda4)."',
        descrizione4       = '".implodeDescrizioni( $domande['domanda4'], $domanda4)."',
        valori5            = '".implodeValori( $domanda5)."',
        descrizione5       = '".implodeDescrizioni( $domande['domanda5'], $domanda5)."',
        domanda6           = '".$domanda6."',
        valori7            = '".implodeValori($domanda7)."',
        descrizione7       = '".implodeDescrizioni( $domande['domanda7'], $domanda7)."',
        domanda8           = '".$domanda8."',
        domanda9           = '".$domanda9."',
        domanda10          = '".$domanda10."',
        domanda11          = '".$domanda11."',
        domanda11_commento = '".$domanda11_commento."',
        domanda12          = '".$domanda12."',
        domanda13          = '".$domanda13."',
        valori14           = '".implodeValori($domanda14)."',
        descrizione14      = '".implodeDescrizioni($domande['domanda14'], $domanda14)."',
        domanda15          = '".$domanda15."',
        domanda16          = '".$domanda16."',
        domanda17          = '".$domanda17."',
        domanda18          = '".$domanda18."',
        domanda19          = '".$domanda19."',
        domanda20          = '".$domanda20."',
        domanda21          = '".$domanda21."',
        domanda22          = '".$domanda22."',
        domanda23          = '".$domanda23."',
        domanda24          = '".$domanda24."',
        domanda25          = '".$domanda25."',
        domanda26          = '".$domanda26."',
        domanda27          = '".$domanda27."'
        WHERE id_utente    = ".$_SESSION['id_utente']."
      ";
      mysql_query($sql);
    } else {
      //Sono in inserimento per la prima volta di un record
      $sql = " INSERT INTO questionario
       (id_utente, valori1, descrizione1, domanda1_altro, domanda2, domanda3, valori4,
        descrizione4, valori5, descrizione5, domanda6, valori7, descrizione7,
        domanda8, domanda9, domanda10, domanda11, domanda11_commento 	, domanda12, domanda13, valori14,
        descrizione14, domanda15, domanda16, domanda17, domanda18, domanda19,
        domanda20, domanda21, domanda22, domanda23, domanda24, domanda25, domanda26, domanda27)
        VALUES (
           ".$_SESSION['id_utente'].",'".implodeValori($domanda1)."',
           '".implodeDescrizioni($domande['domanda1'], $domanda1)."', '".$domanda1_altro."',
           '".$domanda2."', '".$domanda3."', '".implodeValori($domanda4)."',
           '".implodeDescrizioni( $domande['domanda4'], $domanda4)."', '".implodeValori($domanda5)."',
           '".implodeDescrizioni( $domande['domanda5'], $domanda5)."', '".$domanda6."',
           '".implodeValori($domanda7)."', '".implodeDescrizioni( $domande['domanda7'], $domanda7)."',
            '".$domanda8."', '".$domanda9."', '".$domanda10."',
            '".$domanda11."', '".$domanda11_commento."', '".$domanda12."', '".$domanda13."',
            '".implodeValori($domanda14)."', '".implodeDescrizioni( $domande['domanda14'], $domanda14)."',
            '".$domanda15."', '".$domanda16."', '".$domanda17."',
            '".$domanda18."', '".$domanda19."', '".$domanda20."',
            '".$domanda21."', '".$domanda22."', '".$domanda23."',
            '".$domanda24."', '".$domanda25."', '".$domanda26."', '".$domanda27."'
        )
      ";
     // die($sql);
      mysql_query($sql);
    }
    echo "
    <script> window.location.href = 'index-loggato.php'; </script>
    ";
  }

  //Recupero valori di inserimento db
  $campi = db_query_generale( 'questionario', 'id_utente ='.$_SESSION['id_utente'] ,'id_utente');
  $res_campi = mysql_fetch_assoc($campi);

?>

<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="two-third-responsive blue">
                <h1>questionario</h1>
                </div>

                <div class="one-third-responsive last-column red2">
                </div>


                <div class="clear"></div>

                <div class="two-third-responsive supervisor">
                <h1>Questionario Customer Satisfaction 2016</h1>

                <h2 class="colorerosso">Percorsi di informazione</h2>

                <h4 style="padding-top:10px;">1. Società di appartenenza</h4>

                <form class="" action="questionario.php" method="post">

                <input type="hidden" name="id_utente" value="<?php echo $_SESSION['id_utente'] ?>">
                <ul class="questionario">
                <?php
                  $valori1 = explode(";", $res_campi['valori1']);
                  for ($i=1; $i <= count($domande['domanda1']); $i++) {
                    $checked = (in_array($i, $valori1)) ? 'checked' : '' ;
                    $text = $domande['domanda1'][$i];
                    echo '
                      <li><input type="checkbox" name="domanda1[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                    ';
                  }
                ?>
                <li>
                <textarea class="comment" placeholder="Altro ..." name="domanda1_altro"><?php if (isset($res_campi['domanda1_altro'])) echo $res_campi['domanda1_altro']; ?></textarea>
                </li>
                </ul>

                <h4 style="padding-top:10px;">2. Ruolo o funzione all’interno della società</h4>

                <ul class="questionario">
                <li><textarea class="comment" placeholder="Altro ..." name="domanda2"><?php if (isset($res_campi['domanda2'])) echo $res_campi['domanda2']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">3. Anzianità di servizio all’interno della società</h4>

                <ul class="questionario">
                  <li><textarea class="comment" placeholder="Altro ..." name="domanda3"><?php if (isset($res_campi['domanda3'])) echo $res_campi['domanda3']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">4. A quali altri percorsi di informazione ha partecipato?</h4>

                <ul class="questionario">
                  <?php
                    $valori4 = explode(";", $res_campi['valori4']);
                    for ($i=1; $i <= count($domande['domanda4']); $i++) {
                      $checked = (in_array($i, $valori4)) ? 'checked' : '' ;
                      $text = $domande['domanda4'][$i];
                      echo '
                        <li><input type="checkbox" name="domanda4[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                      ';
                    }
                  ?>
                </ul>

               <h4 style="padding-top:10px;">5.	 Qual è il motivo principale che l’ha spinta a partecipare al percorso di informazione? (è possibile selezionare più risposte)</h4>

                <ul class="questionario">
                  <?php
                    $valori5 = explode(";", $res_campi['valori5']);
                    for ($i=1; $i <= count($domande['domanda5']); $i++) {
                      $checked = (in_array($i, $valori5)) ? 'checked' : '' ;
                      $text = $domande['domanda5'][$i];
                      echo '
                        <li><input type="checkbox" name="domanda5[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                      ';
                    }
                  ?>
                </ul>


               <h4 style="padding-top:10px;">6. Ha trovato interessanti gli argomenti affrontati nella tavola rotonda di venerdì?</h4>

               <div class="rateit" id="rateit6" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda6'])) echo $res_campi['domanda6']; ?>"></div>
               <input type="hidden" name="domanda6" id="domanda6" value="<?php if (isset($res_campi['domanda6'])) echo $res_campi['domanda6'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>


                <h4 style="padding-top:10px;">7. Quale è stato il partecipante più incisivo alla tavola rotonda di venerdì?</h4>
                <p><strong>Lato investitori:</strong></p>
                <ul class="questionario">
                  <?php
                    $valori7 = explode(";", $res_campi['valori7']);
                    for ($i=1; $i <= 3; $i++) {
                      $checked = (in_array($i, $valori7)) ? 'checked' : '' ;
                      $text = $domande['domanda7'][$i];
                      echo '
                        <li><input type="checkbox" name="domanda7[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                      ';
                    }
                  ?>
                </ul>
                <p><strong>Lato industria:</strong></p>
                 <ul class="questionario">
                   <?php
                     $valori7 = explode(";", $res_campi['valori7']);
                     for ($i=4; $i <= 7; $i++) {
                       $checked = (in_array($i, $valori7)) ? 'checked' : '' ;
                       $text = $domande['domanda7'][$i];
                       echo '
                         <li><input type="checkbox" name="domanda7[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                       ';
                     }
                   ?>
                </ul>

                <h4 style="padding-top:10px;">8. Complessivamente, cosa NON è stato di suo gradimento nella tavola rotonda?</h4>
                <ul class="questionario">
                  <li><textarea class="comment" placeholder="Altro ..." name="domanda8"><?php if (isset($res_campi['domanda8'])) echo $res_campi['domanda8']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">9. Sempre in merito alla tavola rotonda, complessivamente, cosa ha particolarmente gradito?</h4>
                <ul class="questionario">
                  <li><textarea class="comment" placeholder="Altro ..." name="domanda9"><?php if (isset($res_campi['domanda9'])) echo $res_campi['domanda9']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">10. Ha trovato interessanti gli argomenti affrontati nelle sessioni parallele?</h4>

               <div class="rateit" id="rateit10" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda10'])) echo $res_campi['domanda10']; ?>"></div>
               <input type="hidden" name="domanda10" id="domanda10" value="<?php if (isset($res_campi['domanda10'])) echo $res_campi['domanda10'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                 <h4 style="padding-top:10px;">11. Ha trovato interessante la presentazione introduttiva alla sessione parallela a cura di Prometeia Advisor?</h4>

               <div class="rateit" id="rateit11" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda11'])) echo $res_campi['domanda11']; ?>"></div>
               <input type="hidden" name="domanda11" id="domanda11" value="<?php if (isset($res_campi['domanda11'])) echo $res_campi['domanda11'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>
                 <ul class="questionario">
                   <li><textarea class="comment" placeholder="Eventuale Commento..." name="domanda11_commento"><?php if (isset($res_campi['domanda11_commento'])) echo $res_campi['domanda11_commento']; ?></textarea></li>
                </ul>

               <h4 style="padding-top:10px;">12. Sempre per le sessioni parallele, quale tra i temi affrontati ha particolarmente gradito?</h4>
                <ul class="questionario">
                  <li><textarea class="comment" placeholder="Altro ..." name="domanda12"><?php if (isset($res_campi['domanda12'])) echo $res_campi['domanda12']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">13. Ha trovato interessanti gli argomenti affrontati nella tavola rotonda di sabato?</h4>

               <div class="rateit" id="rateit13" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda13'])) echo $res_campi['domanda13']; ?>"></div>
               <input type="hidden" name="domanda13" id="domanda13" value="<?php if (isset($res_campi['domanda13'])) echo $res_campi['domanda13'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                 <h4 style="padding-top:10px;">14. Quale è stato il partecipante più incisivo alla tavola rotonda?</h4>
                <p><strong>Lato investitori:</strong></p>
                <ul class="questionario">
                  <?php
                    $valori14 = explode(";", $res_campi['valori14']);
                    for ($i=1; $i <= 3; $i++) {
                      $checked = (in_array($i, $valori14)) ? 'checked' : '' ;
                      $text = $domande['domanda14'][$i];
                      echo '
                        <li><input type="checkbox" name="domanda14[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                      ';
                    }
                  ?>
                </ul>
                <p><strong>Lato industria:</strong></p>
                 <ul class="questionario">
                   <?php
                     $valori14 = explode(";", $res_campi['valori14']);
                     for ($i=4; $i <= 7; $i++) {
                       $checked = (in_array($i, $valori14)) ? 'checked' : '' ;
                       $text = $domande['domanda14'][$i];
                       echo '
                         <li><input type="checkbox" name="domanda14[]" value="'.$i.'" '.$checked.' class="checkbox"><span> '.$text.' </span></li>
                       ';
                     }
                   ?>
                </ul>

                 <h4 style="padding-top:10px;">15. Complessivamente, cosa NON è stato di suo gradimento nella tavola rotonda?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda15"><?php if (isset($res_campi['domanda15'])) echo $res_campi['domanda15']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">16. Sempre in merito alla tavola rotonda, complessivamente, cosa ha particolarmente gradito?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda16"><?php if (isset($res_campi['domanda16'])) echo $res_campi['domanda16']; ?></textarea></li>
                </ul>

                 <h2 style="color:#0065a4;">In generale</h2>

                 <h4 style="padding-top:10px;">17. Ha trovato interessante le tematiche affrontate nel convegno “i mercati tra sogni e psicoanalisi”?</h4>

               <div class="rateit" id="rateit17" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda17'])) echo $res_campi['domanda17']; ?>"></div>
               <input type="hidden" name="domanda17" id="domanda17" value="<?php if (isset($res_campi['domanda17'])) echo $res_campi['domanda17'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                 <h4 style="padding-top:10px;">18. Ha trovato interessanti gli interventi dei differenti relatori?</h4>

               <div class="rateit" id="rateit18" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda18'])) echo $res_campi['domanda18']; ?>"></div>
               <input type="hidden" name="domanda18" id="domanda18" value="<?php if (isset($res_campi['domanda18'])) echo $res_campi['domanda18'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                 <h4 style="padding-top:10px;">19. Quali sono secondo lei i temi principali emersi nel corso delle due giornate di lavoro?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda19"><?php if (isset($res_campi['domanda19'])) echo $res_campi['domanda19']; ?></textarea></li>
                </ul>


                 <h4 style="padding-top:10px;">20. Quali degli argomenti vorrebbe approfondire nel corso delle prossime settimane?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda20"><?php if (isset($res_campi['domanda20'])) echo $res_campi['domanda20']; ?></textarea></li>
                </ul>

                <h2 class="colorerosso">Percorso di informazione - organizzazione</h2>

                <h4 style="padding-top:10px;">1. Ha apprezzato la scelta di Vienna come destinazione del percorso di informazione?</h4>

                <ul class="questionario">
                <?php
                  if (isset($res_campi['domanda21']) && $res_campi['domanda21'] == 1) {
                    echo '
                    <li><input type="radio" class="checkbox" name="domanda21" value="1" checked><span>Sì</span></li>
                    <li><input type="radio" class="checkbox" name="domanda21" value="0"><span>No</span></li>
                    ';
                  } else if (isset($res_campi['domanda21']) && $res_campi['domanda21'] == 0) {
                    echo '
                    <li><input type="radio" class="checkbox" name="domanda21" value="1"><span>Sì</span></li>
                    <li><input type="radio" class="checkbox" name="domanda21" value="0" checked><span>No</span></li>
                    ';
                  } else {
                    echo '
                    <li><input type="radio" class="checkbox" name="domanda21" value="1" checked><span>Sì</span></li>
                    <li><input type="radio" class="checkbox" name="domanda21" value="0"><span>No</span></li>
                    ';
                  }
                ?>

                </ul>

                <h4 style="padding-top:10px;">2. Ha riscontrato cordialità da parte della segreteria organizzativa nella corrispondenza e nei contatti telefonici?</h4>
                 <div class="rateit" id="rateit22" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda22'])) echo $res_campi['domanda22']; ?>"></div>
                 <input type="hidden" name="domanda22" id="domanda22" value="<?php if (isset($res_campi['domanda22'])) echo $res_campi['domanda22'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                  <h4 style="padding-top:10px;">3. Le risposte della segreteria organizzativa sono state tempestive ed esaurienti?</h4>
                 <div class="rateit" id="rateit23" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda23'])) echo $res_campi['domanda23']; ?>"></div>
                 <input type="hidden" name="domanda23" id="domanda23" value="<?php if (isset($res_campi['domanda23'])) echo $res_campi['domanda23'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                  <h4 style="padding-top:10px;">4.	Ha trovato funzionale la zona dedicata al convegno?</h4>
                 <div class="rateit" id="rateit24" data-rateit-min="0" data-rateit-value="<?php if (isset($res_campi['domanda24'])) echo $res_campi['domanda24']; ?>"></div>
                 <input type="hidden" name="domanda24" id="domanda24" value="<?php if (isset($res_campi['domanda24'])) echo $res_campi['domanda24'] ?>">
                <div>
                <span id="value"></span><span id="hover5"></span>
                 </div>

                 <h4 style="padding-top:10px;">5. Quali sono secondo lei i punti di forza dell’organizzazione dei nostri percorsi di informazione?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda25"><?php if (isset($res_campi['domanda25'])) echo $res_campi['domanda25']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">6. Ha dei suggerimenti per migliorare l’organizzazione dei nostri percorsi di informazione?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda26"><?php if (isset($res_campi['domanda26'])) echo $res_campi['domanda26']; ?></textarea></li>
                </ul>

                <h4 style="padding-top:10px;">7. In quale città europea vorrebbe che avesse luogo il prossimo percorso di informazione?</h4>
                  <ul class="questionario">
                    <li><textarea class="comment" placeholder="Altro ..." name="domanda27"><?php if (isset($res_campi['domanda27'])) echo $res_campi['domanda27']; ?></textarea></li>
                </ul>

                <script type="text/javascript">
			              $(function () { $('#rateit5').rateit({ max: 10, step: 1 }); });
                    $("#rateit6").bind('rated', function (event, value) {
                      $('#domanda6').val(value);
                    });
                    $("#rateit10").bind('rated', function (event, value) {
                      $('#domanda10').val(value);
                    });
                    $("#rateit11").bind('rated', function (event, value) {
                      $('#domanda11').val(value);
                    });
                    $("#rateit13").bind('rated', function (event, value) {
                      $('#domanda13').val(value);
                    });
                    $("#rateit17").bind('rated', function (event, value) {
                      $('#domanda17').val(value);
                    });
                    $("#rateit18").bind('rated', function (event, value) {
                      $('#domanda18').val(value);
                    });
                    $("#rateit22").bind('rated', function (event, value) {
                      $('#domanda22').val(value);
                    });
                    $("#rateit23").bind('rated', function (event, value) {
                      $('#domanda23').val(value);
                    });
                    $("#rateit24").bind('rated', function (event, value) {
                      $('#domanda24').val(value);
                    });
                </script>

			          <script src="scripts/jquery.rateit.js" type="text/javascript"></script>

                </div>
                <div class="one-third-responsive last-column"></div>
                <div class="clear"></div>
                </div>
            <?php include "footer.php" ?>

        </div>
        <div class="valutazione">
          <button type="submit" class="button button-red button-round" name="inviaQuestionario">Invia</button>
        </div>
        </form>
</body>
