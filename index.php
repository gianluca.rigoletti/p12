<?php
  $classeBody = "login";
  $no_header  = true;
  $title      = "Login";
  include "header.php";
?>

<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <img src="immagini/logo-prometeia.png" width="100" />
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="one-half-responsive"></div>

                <div class="one-half-responsive last-column">
                <img class="responsive-image half-bottom" src="immagini/logo-login.png" />


                </div>

                <div class="one-half-responsive"> </div>

                <div class="one-half-responsive last-column">

               <h1 class="title-login">i mercati tra sogno e psicoanalisi</h1>

                </div>

                <div class="clear"></div>

                <div class="one-half-responsive">
                <div class="page-login full-bottom material-container">
                <p class="light">LOGIN</p>
                <?php if (isset($_GET['error']) && $_GET['error']): ?>
                  <!-- Login fallito -->
                  <div class="">
                    Credenziali errate
                  </div>
                <?php endif; ?>
                <form id="login-form" action="index.php" method="post">
                    <div class="login-input">
                        <i class="fa fa-user"></i>
                        <input type="tel" id="user" name="user" placeholder="Username" onfocus="" onblur="" value="<?php if(isset($_POST['user'])) echo $_POST['user']; ?>">
                    </div>
                    <img class="responsive-image half-bottom" src="immagini/framework/shadow.png" />
                    <div class="login-password">
                        <i class="fa fa-lock"></i>
                        <input type="tel" id="password" name="password" placeholder="Password" onfocus="" onblur="">
                    </div>
                    <img class="responsive-image half-bottom" src="immagini/framework/shadow.png" />
                    <button type="submit" class="login-button button button-small button-red button-fullscreen full-bottom">Effettua il Login</button>
                </form>
                </div>

                </div>
                <div class="one-half-responsive last-column"></div>

            </div>
        </div>

        <script>
          $(document).ready( function(){
            $('#login-form').validate({
              submitHandler: function(form) {
                form.submit();
              },
              rules: {
                user: {
                  required: true,
                  digits: true
                },
                password: {
                  required: true ,
                  digits: true
                }
              }
            });
          });
        </script>



</body>
