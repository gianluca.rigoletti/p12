<?php
  $classeBody = "";
  $title      = "Televoto";
  $no_header  = true;
  include "header.php";

  //Recupero dei dati sulla domanda attiva
  $domanda     = db_query_generale('televoto', ' attiva = 1 ', 'id');
  $res_domanda = mysql_fetch_assoc($domanda);
  $risposte_date  = db_query_count('risposte_televoto', ' id_utente = '.$_SESSION['id_utente'].' AND id_domanda = '.$res_domanda['ID']);

  //Controllo se l'utente ha già dato la risposta sulla domanda attiva
  //In caso affermativo lo indirizzo ad attesatelevoto
  if ($risposte_date != 0) {
    echo "
      <script> window.location.href = 'attesatelevoto.php'; </script>
    ";
  }

  //Controllo sull'inserimento della risposta
  if (isset($_POST['risposta_data']) and !db_is_null($_POST['risposta_data'])) {
    $sql = " INSERT INTO risposte_televoto (id, id_utente, id_domanda, risposta)
             VALUES (null, ".$_SESSION['id_utente'].", ".$res_domanda['ID'].", ".$_POST['risposta_data'].") ";
    mysql_query($sql);
    //Giusto per testare
    if(mysql_errno()){
      echo "MySQL error ".mysql_errno().": "
         .mysql_error()."\n<br>When executing <br>\n$sql\n<br>";
    } else {
      echo "
        <script> window.location.href = 'attesatelevoto.php' </script>
      ";
    }
  }

?>

<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="one-third-responsive">
                <img src="immagini/logo.png" width="220" />
                </div>

                <div class="two-third-responsive last-column tv">
                <h1><?php echo $res_domanda['domanda']; ?></h1>
                </div>
     <div class="clear"></div>

     <button id="bott" class="question01"><?php echo $res_domanda['risposta_1']; ?></button>

          <div class="clear"></div>

     <button id="bott2" class="question02"><?php echo $res_domanda['risposta_2']; ?></button>

          <div class="clear"></div>

     <button id="bott3" class="question03"><?php echo $res_domanda['risposta_3']; ?></button>

          <div class="clear"></div>

     <button id="bott4" class="question04"><?php echo $res_domanda['risposta_4']; ?></button>


<script type="text/javascript">

  var risposta_data;
  $(function(){
    $("#bott").click(function(){
      $(".conferma").addClass("conferma_ok");
      $('#risposta_data').val(1);
    });
  });

  $(function(){
    $("#bott2").click(function(){
      $(".conferma").addClass("conferma_ok");
      $('#risposta_data').val(2);
    });
  });

  $(function(){
    $("#bott3").click(function(){
      $(".conferma").addClass("conferma_ok");
      $('#risposta_data').val(3);
    });
  });

  $(function(){
    $("#bott4").click(function(){
      $(".conferma").addClass("conferma_ok");
      $('#risposta_data').val(4);
    });
  });



</script>

            <div class="clear"></div>

            <form class="" action="televoto.php" method="post">
              <div class="one-third-responsive conferma">
              <input type="hidden" id="risposta_data" name="risposta_data" value="">
              <button class="button button-red button-round left-button" >Conferma</button>
              </div>
            </form>

            <div class="one-third-responsive"></div>


                <div class="one-third-responsive last-column ">
                <a class="button button-red button-round right-button" href="index-loggato.php">homepage</a>
                </div>

                <div class="clear"></div>




            </div>

        </div>



</body>
