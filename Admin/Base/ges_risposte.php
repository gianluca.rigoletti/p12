<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
$Tavola= "risposte_televoto";

$ordine = false;

if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Risposta";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Risposta";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

   $cur_rec['ID'] = $_POST['ID'];
   $cur_rec['id_utente'] = $_POST['id_utente'];
   $cur_rec['id_domanda'] = $_POST['id_domanda'];
   $cur_rec['risposta'] = $_POST['risposta'];

   if ( $_POST['id_utente'] == null || $_POST['id_utente'] == " ") {
      $c_err->add("Campo id_utente Obbligatorio","id_utente");
   }
   if ( $_POST['id_domanda'] == null || $_POST['id_domanda'] == " ") {
      $c_err->add("Campo id_domanda Obbligatorio","id_domanda");
   }
   if ( $_POST['risposta'] == null || $_POST['risposta'] == " ") {
      $c_err->add("Campo risposta Obbligatorio","risposta");
   }
   // controllo dup-Val
   if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
        $c_err->add("Risposta Gi&agrave; Codificata","ID");
   }

   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST);
       }  else {
	          db_update($Tavola,$_POST['ID'],$_POST);
       }
       header('Location: vis_risposte.php');
       exit;
   }
}

// torno indietro
$indietro = "vis_risposte.php";
if ($ordine) $indietro .= "?p_ordine=1";
if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
?>

          <script type="text/javascript">

               var validator;
               $().ready(function($) {

                 validator = $("#formG").validate({
                    submitHandler: function(form) {
                        form.submit();
                    } ,
                    rules: {
                           risposta: {
                             required:true,
                             digits: true,
                             range: [1,4]
                           },
                           id_utente: {required: true},
                           id_domanda: {required: true}
                          }
                	});
               });
          	</script>

        <form id="formG" action="" method="post">
        <table width="100%" border=0>
           <tr><td class="px" height="30"></td></tr>
           <tr><td align="center">
           <table width="95%" border=0>

            <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

            <tr>
            <td class="Label" width="15%"> Utente </td>
            <td width="85%">
                <select name="id_utente">
                  <?php
                    if (isset($cur_rec['id_utente'])) {
                      db_html_select_cod('user', 'ID', 'utente', true, null);
                    } else {
                      db_html_select_cod('user', '', 'ID', 'utente', true, null);
                    }
                  ?>
                </select>
            </td>
            </tr>

            <tr>
            <td class="Label" width="15%"> Domanda </td>
            <td width="85%">
                <select name="id_domanda">
                  <?php
                    if (isset($cur_rec['id_domanda'])) {
                      db_html_select_cod('televoto', 'ID', 'domanda', true, null);
                    } else {
                      db_html_select_cod('televoto', '', 'ID', 'domanda', true, null);
                    }
                  ?>
                </select>
            </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta </td>
            <td width="20%">
                <select name="risposta" id="risposta">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
            </td>
            </tr>

            <tr><td colspan=2 class="px" height="20"></td></tr>
            <tr>
            <td colspan=2 align="center">
               <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
            </td>
            <td></td>
            </tr>


        </table>
        </td></tr></table>
        </form>



<?php require '../../Librerie/ges_html_bot.php';


?>
