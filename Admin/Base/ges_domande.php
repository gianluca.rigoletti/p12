<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
$Tavola= "televoto";

$ordine = false;

if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Domanda";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuova Domanda";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

   $cur_rec['ID'] = $_POST['ID'];
   $cur_rec['ordine'] = $_POST['ordine'];
   $cur_rec['domanda'] = $_POST['domanda'];
   $cur_rec['risposta_1'] = $_POST['risposta_1'];
   $cur_rec['risposta_1_a'] = $_POST['risposta_1_a'];
   $cur_rec['risposta_1_b'] = $_POST['risposta_1_b'];
   $cur_rec['risposta_1_c'] = $_POST['risposta_1_c'];
   $cur_rec['risposta_2'] = $_POST['risposta_2'];
   $cur_rec['risposta_2_a'] = $_POST['risposta_2_a'];
   $cur_rec['risposta_2_b'] = $_POST['risposta_2_b'];
   $cur_rec['risposta_2_c'] = $_POST['risposta_2_c'];
   $cur_rec['risposta_3'] = $_POST['risposta_3'];
   $cur_rec['risposta_3_a'] = $_POST['risposta_3_a'];
   $cur_rec['risposta_3_b'] = $_POST['risposta_3_b'];
   $cur_rec['risposta_3_c'] = $_POST['risposta_3_c'];
   $cur_rec['risposta_4'] = $_POST['risposta_4'];
   $cur_rec['risposta_4_a'] = $_POST['risposta_4_a'];
   $cur_rec['risposta_4_b'] = $_POST['risposta_4_b'];
   $cur_rec['risposta_4_c'] = $_POST['risposta_4_c'];
   $cur_rec['risposta_esatta'] = $_POST['risposta_esatta'];
   //die(var_dump( $_POST['attiva']));
   if (isset($_POST['attiva']) && ! db_is_null($_POST['attiva'])) {
     $cur_rec['attiva'] = 1;
     $_POST['attiva']   = 1;
   } else {
     $cur_rec['attiva'] = 0;
     $_POST['attiva']   = 0;
   }
   //die(var_dump($cur_rec['attiva']));

   if ( $_POST['ordine'] == null || $_POST['ordine'] == " ") {
      $c_err->add("Campo ordine Obbligatorio","ordine");
   }
   if ( $_POST['domanda'] == null || $_POST['domanda'] == " ") {
      $c_err->add("Campo domanda Obbligatorio","domanda");
   }
   if ( $_POST['risposta_1'] == null || $_POST['risposta_1'] == " ") {
      $c_err->add("Campo risposta_1 Obbligatorio","risposta_1");
   }
   if ( $_POST['risposta_2'] == null || $_POST['risposta_2'] == " ") {
      $c_err->add("Campo risposta_2 Obbligatorio","risposta_2");
   }
   if ( $_POST['risposta_3'] == null || $_POST['risposta_3'] == " ") {
      $c_err->add("Campo risposta_3 Obbligatorio","risposta_3");
   }
   if ( $_POST['risposta_4'] == null || $_POST['risposta_4'] == " ") {
      $c_err->add("Campo risposta_4 Obbligatorio","risposta_4");
   }
   if ( $_POST['risposta_esatta'] == null || $_POST['risposta_esatta'] == " ") {
      $c_err->add("Campo risposta_esatta Obbligatorio","risposta_esatta");
   }
   // controllo dup-Val
   if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
        $c_err->add("Domanda Gi&agrave; Codificata","ID");
   }

   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST);
       }  else {
	          db_update($Tavola,$_POST['ID'],$_POST);
       }
       header('Location: vis_domande.php');
       exit;
   }
}

// torno indietro
$indietro = "vis_domande.php";
if ($ordine) $indietro .= "?p_ordine=1";
if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
?>

          <script type="text/javascript">

               var validator;
               $().ready(function($) {

                 validator = $("#formG").validate({
                    submitHandler: function(form) {
                        form.submit();
                    } ,
                    rules: {
                           ordine: {
                             required:true,
                             digits: true
                           },
                           domanda: {required: true},
                           risposta_1: {required: true},
                           risposta_2: {required: true},
                           risposta_3: {required: true},
                           risposta_4: {required: true},
                           risposta_esatta: {
                             required: true,
                             range: [1, 4]
                           }

                          }
                	});
               });
          	</script>

        <form id="formG" action="" method="post">
        <table width="100%" border=0>
           <tr><td class="px" height="30"></td></tr>
           <tr><td align="center">
           <table width="95%" border=0>

            <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
            <tr>
            <td class="Label" width="15%"> Ordine </td>
            <td width="85%">
                <input type="number" name="ordine" id="ordine" <?php $c_err->tooltip("ordine");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['ordine']; ?>" >
            </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Domanda </td>
            <td width="20%">
                <textarea id="domanda" name="domanda" ROW="2" COLUMN="150"><?php  if (isset($cur_rec)) echo $cur_rec['domanda'];?></textarea>
            </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta 1 </td>
            <td width="20%">
                <textarea id="risposta_1" name="risposta_1" ROW="2" COLUMN="150"><?php  if (isset($cur_rec)) echo $cur_rec['risposta_1'];?></textarea>
            </td>
            </tr>

            <tr>
              <td class="Label" width="20%"> Risposta 1 </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_1_a" id="risposta_1_a" style="width:60%;" <?php $c_err->tooltip("risposta_1_a");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_1_a']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_1_b" id="risposta_1_b" style="width:60%;" <?php $c_err->tooltip("risposta_1_b");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_1_b']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_1_c" id="risposta_1_c" style="width:60%;" <?php $c_err->tooltip("risposta_1_c");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_1_c']; ?>" >
              </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta 2 </td>
            <td width="20%">
                <textarea id="risposta_2" name="risposta_2" ROW="2" COLUMN="150"><?php  if (isset($cur_rec)) echo $cur_rec['risposta_2'];?></textarea>
            </td>
            </tr>

            <tr>
              <td class="Label" width="20%"> Numero Risposta 2 </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_2_a" id="risposta_2_a" style="width:60%;" <?php $c_err->tooltip("risposta_2_a");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_2_a']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_2_b" id="risposta_2_b" style="width:60%;" <?php $c_err->tooltip("risposta_2_b");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_2_b']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_2_c" id="risposta_2_c" style="width:60%;" <?php $c_err->tooltip("risposta_2_c");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_2_c']; ?>" >
              </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta 3 </td>
            <td width="20%">
                <textarea id="risposta_3" name="risposta_3" ROW="2" COLUMN="150"><?php  if (isset($cur_rec)) echo $cur_rec['risposta_3'];?></textarea>
            </td>
            </tr>

            <tr>
              <td class="Label" width="20%"> Numero Risposta 3 </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_3_a" id="risposta_3_a" style="width:60%;" <?php $c_err->tooltip("risposta_3_a");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_3_a']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_3_b" id="risposta_3_b" style="width:60%;" <?php $c_err->tooltip("risposta_3_b");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_3_b']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_3_c" id="risposta_3_c" style="width:60%;" <?php $c_err->tooltip("risposta_3_c");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_3_c']; ?>" >
              </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta 4 </td>
            <td width="20%">
                <textarea id="risposta_4" name="risposta_4" ROW="2" COLUMN="150"><?php  if (isset($cur_rec)) echo $cur_rec['risposta_4'];?></textarea>
            </td>
            </tr>

            <tr>
              <td class="Label" width="20%"> Numero Risposta 4 </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_4_a" id="risposta_4_a" style="width:60%;" <?php $c_err->tooltip("risposta_4_a");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_4_a']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_4_b" id="risposta_4_b" style="width:60%;" <?php $c_err->tooltip("risposta_4_b");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_4_b']; ?>" >
              </td>
              <td style="display:inline-block; width:20%;">
                <input type="number" name="risposta_4_c" id="risposta_4_c" style="width:60%;" <?php $c_err->tooltip("risposta_4_c");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_4_c']; ?>" >
              </td>
            </tr>

            <tr>
            <td class="Label" width="20%"> Risposta Esatta </td>
            <td width="20%">
                <input type="number" name="risposta_esatta" id="risposta_esatta" <?php $c_err->tooltip("risposta_esatta");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['risposta_esatta']; ?>" >
            </td>
            </tr>

            <tr>
              <td class="Label" width="35%"> Attiva </td>
              <td width="65%">
                  <input type="checkbox" <?php $c_err->tooltip("attiva"); ?> name="attiva" id="attiva" value="1" <?php if (isset($cur_rec['attiva']) && $cur_rec['attiva'] == 1 ) echo "checked"; ?>>
              </td>
            </tr>

            <tr><td colspan=2 class="px" height="20"></td></tr>
            <tr>
            <td colspan=2 align="center">
               <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
            </td>
            <td></td>
            </tr>


        </table>
        </td></tr></table>
        </form>



<?php require '../../Librerie/ges_html_bot.php';


?>
