<?php

require '../../Librerie/connect.php';

$Titolo = "Gestione Utenti";
$Tavola= "user";

//*******************************************************
//FILTRI
$where = "";
$and = "";
if (isset($_POST['nome'])) {
      $where .= "upper(nome) like upper('".str_replace("'","\'",str_replace("*","",$_POST['nome']))."%') ";
}

if (db_is_null($where)) $where = " 1 = 1";

//*****************************************************************

 $risultato = db_query_generale($Tavola,$where,"id");

require '../../Librerie/ges_html_top.php';
require '../../Librerie/html.php';

?>

<script>
  $(function() {
    $( "#nome" ).autocomplete({
      source: "autocomplete.php?tavola=utenti&campo=nome" //creo un file.php dove li passo pr ogni tavola
    });
  });

  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>

       <form action="" method="post">
       <table>
            <tr><td colspan="8" class="px" height="30"></td></tr>

            <tr>

            <td class="Label"> Utente</td>
            <td >
                <input type="text" <?php $c_err->tooltip("utente");?>  name="utente" id="utente" size="20" />
            </td>

            <td class="Label"> Nome </td>
            <td >
                <input type="text" <?php $c_err->tooltip("nome");?>  value="<?php  /*if (isset($_POST['descrizione'])) echo $_POST['descrizione'];*/ ?>" name="nome" id="nome" size="20" />
            </td>

            <td>
               <input type="submit" name="VIS" value="Visualizza" onchange="this.form.submit()">
            </td>
            </tr>

            <tr><td colspan="8" class="px" height="10"></td></tr>
            <tr><td valign="middle" width="16px ">
              <a href="ges_utenti.php?p_upd=0"><img src="../../Icons/add.png"> </a>
            </td>
            <td valign="middle"><a href="ges_utenti.php?p_upd=0">Inserisci Nuovo Utente</a> </td>
            </tr>
            <tr><td valign="middle" width="16px ">
            <tr><td colspan="8" class="px" height="20"></td></tr>
       </table>
       </form>

           <table width="100%" class="display" id="tabellavis">
           <thead>
            <tr>
            <th width="30%"> Id </td>
            <th  width="40%"> Nome </td>
            <th  width="30%"> Utente </td>
            <th  width="30%"> Categoria </td>
            <th  width="5%"> &nbsp;</td>
            <th  width="5%"> &nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php

            while ($cur_rec = mysql_fetch_assoc($risultato))
            {

                 echo "<tr>
                        <td >".$cur_rec['ID']."   </td>
                        <td >".$cur_rec['nome']."   </td>
                        <td >".$cur_rec['utente']."   </td>
                        <td >".$cur_rec['categoria']."   </td>
                        ";
                 echo "</td>
                 <td ><a href=\"ges_utenti.php?p_upd=1&p_id=".$cur_rec['ID']."\"><img class=\"link\" src=\"../../Icons/edit.gif\" title=\"modifica la riga\" /></a></td>
                        <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_utenti')\"><img class=\"link\" src=\"../../Icons/elimina.jpg\" title=\"cancella la riga\"/></a></td>
                    </tr>";

            }
            ?>
            </tbody>
        </table>

                    <div id="cancella" title="Cancella Utente">
               </div>

            <script type="text/javascript" charset="utf-8">
              $(document).ready(function() {
                $('#tabellavis').dataTable({
                "bJQueryUI": true,
            "sPaginationType": "full_numbers",
                "bFilter":true,
                "aoColumns": [
                            null,
                            null,
                            null,
                            null,
                            { "bSortable": false },
                            { "bSortable": false }
                        ]

                 });
               $("#dettaglio").dialog({
                 autoOpen: false,
                 height: 'auto',
                 minHeight: 180,
                 maxHeight: 600,
                 width : 300,
                 minWidth: 300,
                 maxWidth: 300,
                 modal: true,
                 buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }}
                 } );
              } );
              function dett(id) {
                 var htmlStr = $('#dettaglio'+id).html();
                 $("#dettaglio").html(htmlStr);
                 $("#dettaglio").dialog("open");
              }
            </script>
                    <tr><td class="px" height="20"></td></tr>
                    <table >
                    <tr><td class="px" height="100"></td></tr>
                    </table>
                </td></tr></table>
        <?php require '../../Librerie/ges_html_bot.php'; ?>
