<?php

require '../../Librerie/connect.php';
require '../../Librerie/html.php';
$Tavola= "user";

$ordine = false;

if ($_GET['p_upd']==1) {
   $Funzione = "Update";
   $Disabilita_chiave = "disabled";
   $Titolo = "Modifica Utente";
} else {
   $Funzione = "Insert";
   $Disabilita_chiave = "";
   $Titolo = "Nuovo Utente";
}

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
   $risultato = db_query_mod($Tavola,$_GET['p_id']);
   $cur_rec = mysql_fetch_assoc($risultato);
}

// confermo
if ( isset($_POST['Insert']) || isset($_POST['Update']) ) {

   $cur_rec['ID']        = $_POST['ID'];
   $cur_rec['utente']    = $_POST['utente'];
   $cur_rec['password']  = $_POST['password'];
   $cur_rec['nome']      = $_POST['nome'];
   $cur_rec['categoria'] = $_POST['categoria'];

   if ( $_POST['utente'] == null || $_POST['utente'] == " ") {
      $c_err->add("Campo Utente Obbligatorio","utente");
   }

   if ( $_POST['password'] == null || $_POST['password'] == " ") {
      $c_err->add("Campo Password Obbligatorio","password");
   }

   if ( $_POST['nome'] == null || $_POST['nome'] == " ") {
      $c_err->add("Campo Nome Obbligatorio","nome");
   }

   if ( $_POST['categoria'] == null || $_POST['categoria'] == " ") {
      $c_err->add("Campo Categoria Obbligatorio","categoria");
   }

   // controllo dup-Val
   if ( isset($_POST['Insert']) && db_dup_key($Tavola,$_POST) > 0 )  {
        $c_err->add("Utente Gi&agrave; Codificato","utente");
   }

   if (!$c_err->is_errore()) {
       if ( isset($_POST['Insert'])) {
	          db_insert($Tavola,$_POST);
       }  else {
	          db_update($Tavola,$_POST['ID'],$_POST);
       }
       header('Location: vis_utenti.php');
       exit;
   }
}

// torno indietro
$indietro = "vis_utenti.php";
if ($ordine) $indietro .= "?p_ordine=1";
if (isset($_POST['Return'])) {
   header("Location: ".$indietro);
   exit;
}

require '../../Librerie/ges_html_top.php';

$c_err->mostra();
?>

          <script type="text/javascript">

               var validator;
               $().ready(function($) {

                 validator = $("#formG").validate({
                    submitHandler: function(form) {
                        form.submit();
                    } ,
                    rules: {
                           utente: {
                             required:true,
                             digits: true
                           },
                           password: {
                             required:true,
                             digits: true
                           },
                           nome: {
                             required:true
                           },
                           categoria: {
                             required: true,
                             range: [1,3]
                           }
                          }
                	});
               });
          	</script>

        <form id="formG" action="" method="post">
        <table width="100%" border=0>
           <tr><td class="px" height="30"></td></tr>
           <tr><td align="center">
           <table width="95%" border=0>

            <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
            <tr>
            <td class="Label" width="15%"> Utente </td>
            <td width="85%">
                <input type="text" name="utente" id="utente" <?php $c_err->tooltip("utente");?> value="<?php  if (isset($cur_rec)) echo $cur_rec['utente']; ?>" >
            </td>
            </tr>

             <tr>
            <td class="Label" width="15%"> Password </td>
            <td width="85%">
                <input type="text" <?php $c_err->tooltip("password");?> name="password" id="password" value="<?php  if (isset($cur_rec)) echo ($cur_rec['password']); ?>" >
            </td>
            </tr>

            <tr>
            <td class="Label" width="15%"> Nome </td>
            <td width="85%">
                <input type="text" <?php $c_err->tooltip("nome");?> name="nome" id="nome" value="<?php  if (isset($cur_rec)) echo ($cur_rec['nome']); ?>"  size="60" maxlength="500" >
            </td>
            </tr>

            <tr>
            <td class="Label" width="15%"> Categoria </td>
            <td width="85%">
                <input type="number" <?php $c_err->tooltip("categoria");?> name="categoria" id="categoria" value="<?php  if (isset($cur_rec)) echo ($cur_rec['categoria']); ?>"  size="60" maxlength="500" >
            </td>
            </tr>

            <tr><td colspan=2 class="px" height="20"></td></tr>
            <tr>
            <td colspan=2 align="center">
               <button class="cancel" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
            </td>
            <td></td>
            </tr>


        </table>
        </td></tr></table>
        </form>



<?php require '../../Librerie/ges_html_bot.php';


?>
