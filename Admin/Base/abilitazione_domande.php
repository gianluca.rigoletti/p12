<?php

require '../../Librerie/connect.php';

$Titolo = "Gestione Abilitazione Domande Televoto";
$Tavola= "televoto";

//*******************************************************
//FILTRI
$where = "";
$and = "";
if (isset($_POST['domanda'])) {
      $where .= "upper(domanda) like upper('".str_replace("'","\'",str_replace("*","",$_POST['domanda']))."%') ";
}

if (db_is_null($where)) $where = " 1 = 1";

//*****************************************************************

// Abilitazione domanda
if (isset($_GET['attiva_domanda']) && ! db_is_null($_GET['attiva_domanda']) && isset($_GET['id']) && ! db_is_null($_GET['id'])) {
  $sql_1 = " UPDATE televoto SET attiva = 0 WHERE ID <> ".$_GET['id'];
  $sql_2 = " UPDATE televoto SET attiva = 1 WHERE ID =  ".$_GET['id'];
  mysql_query($sql_1);
  mysql_query($sql_2);
}

 $risultato = db_query_generale($Tavola,$where,"id");

require '../../Librerie/ges_html_top.php';
require '../../Librerie/html.php';

?>

<script>
  $(function() {
    $( "#domanda" ).autocomplete({
      source: "autocomplete.php?tavola=televoto&campo=domanda" //creo un file.php dove li passo pr ogni tavola
    });
  });

  function conferma(a) {
     if (a.value != null && a.value.length != 0 ) {
        a.form.submit();
     }
  }

 </script>

       <form action="" method="post">
       <table>
            <tr><td colspan="8" class="px" height="30"></td></tr>

            <tr>
            <td class="Label"> Domanda </td>
            <td >
                <input type="text" <?php $c_err->tooltip("domanda");?>  value="<?php  /*if (isset($_POST['descrizione'])) echo $_POST['descrizione'];*/ ?>" name="domanda" id="domanda" size="20" />
            </td>

            <td>
               <input type="submit" name="VIS" value="Visualizza" onchange="this.form.submit()">
            </td>
            </tr>

            <tr><td colspan="8" class="px" height="10"></td></tr>
            <tr><td valign="middle" width="16px ">
            <tr><td colspan="8" class="px" height="20"></td></tr>
       </table>
       </form>

           <table width="100%" class="display" id="tabellavis">
           <thead>
            <tr>
            <th  width="20%"> Id </td>
            <th  width="20%"> Ordine </td>
            <th  width="40%"> Domanda </td>
            <th  width="20%"> Stato </td>
            <th  width="20%"> Visualizza percentuali </td>
            </tr>
            </thead>
            <tbody>
            <?php

            while ($cur_rec = mysql_fetch_assoc($risultato))
            {

                 echo "<tr>
                        <td >".$cur_rec['ID']."   </td>
                        <td >".$cur_rec['ordine']."   </td>
                        <td >".$cur_rec['domanda']."   </td>";
                 echo "
                 <td><a href='abilitazione_domande.php?attiva_domanda=1&id=".$cur_rec['ID']."'>";
                 if ($cur_rec['attiva'] == 1) {
                   echo " <i class='fa fa-2x fa-check-circle' aria-hidden='true'></i> ";
                 } else {
                   echo " <i class='fa fa-2x fa-circle' aria-hidden='true'></i> ";
                 }
                 echo " </a></td>";
                 echo "
                 <td><a href='../../percentuali.php?id=".$cur_rec['ID']."'>
                 <i class='fa fa-2x fa-question-circle' aria-hidden='true'></i>
                 </a></td>";
            }
            ?>
            </tbody>
        </table>

                    <div id="cancella" title="Cancella Domanda">
               </div>

            <script type="text/javascript" charset="utf-8">
              $(document).ready(function() {
                $('#tabellavis').dataTable({
                "bJQueryUI": true,
            "sPaginationType": "full_numbers",
                "bFilter":true,
                "aoColumns": [
                            null,
                            null,
                            null,
                            { "bSortable": false },
                            { "bSortable": false }
                        ]

                 });
               $("#dettaglio").dialog({
                 autoOpen: false,
                 height: 'auto',
                 minHeight: 180,
                 maxHeight: 600,
                 width : 300,
                 minWidth: 300,
                 maxWidth: 300,
                 modal: true,
                 buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }}
                 } );
              } );
              function dett(id) {
                 var htmlStr = $('#dettaglio'+id).html();
                 $("#dettaglio").html(htmlStr);
                 $("#dettaglio").dialog("open");
              }
            </script>
                    <tr><td class="px" height="20"></td></tr>
                    <table >
                    <tr><td class="px" height="100"></td></tr>
                    </table>
                </td></tr></table>
        <?php require '../../Librerie/ges_html_bot.php'; ?>
