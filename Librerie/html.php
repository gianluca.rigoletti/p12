<?php

  function CalendarPopup($name,$day,$month,$year,$class='',$refresh='',$disable=false)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        $dis = ""; 
        if ($disable) $dis = "disabled";
        
        echo "\t <select name=\"".$name."_D\" id=\"".$name."_D\" ".$dis." class=\"$class\" $refresh>\n";
        for( $i = 1; $i <= 31; $i++ ) {
            $selected = "";
            if($i == $day) {
                $selected = "selected";
            }
            echo "\t\t <option value=\"$i\" $selected >$i</option>\n";
        }
        echo "\t </select>\n";

        CalendarPopupMA($name,$month,$year,$class,$refresh,false,$disable);
    }

  function CalendarPopupMA($name,$month,$year,$class='',$refresh='',$solo = true,$disable=false)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        $dis = ""; 
        if ($disable) $dis = "disabled";
                
        if ($solo) {
           echo "\t <input type=\"hidden\" name=\"".$name."_D\" id=\"".$name."_D\" value=\"1\">\n";
        }
        
        echo "\t <select name=\"".$name."_M\" id=\"".$name."_M\"  ".$dis."  class=\"$class\" $refresh>\n";
        for( $i = 1; $i <= 12; $i++ ) {
            $selected = "";
            if($i == $month) {
                $selected = "selected";
            }
            $month_name = ucwords(strftime("%B", mktime (0,0,0,$i,1,0)));
            echo "\t\t <option value=\"$i\"  $selected >$month_name</option>\n";
        }
        echo "\t </select>\n";
        echo "\t <input type=\"text\" name=\"".$name."_Y\"  ".$dis."  id=\"".$name."_Y\" value=\"".$year."\" class=\"$class\"  maxlength=\"4\" size=\"4\" $refresh />\n ";
        if (!$disable) {
           echo "\t <A HREF=\"#\" onClick=\"setDate('$name'); return false;\" TITLE=\"Calendario\" NAME=\"anchor\" ID=\"anchor\">\n";
           echo "\t<img border=\"0\" src=\"../../Icons/cal.png\"></A>\n";
        }
    }

  function decodifica_valore($array,$valore) {
       if (array_key_exists($valore,$array))  return $array[$valore];
       return "";
  }
  
  function dec_selezioni($valore) {
      return lista_selezioni(null,$valore,null,null,true);
  }
  function lista_selezioni($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('I' => 'Inizia con',
                        'U' => 'Uguale a',
                        'C' => 'Contiene');
                        
        if ($dec) return decodifica_valore($valori,$valore);
         
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_magazzini($name,$valore,$class='',$refresh='',$dec=false,$disabled="")
    {
        $valori = array('' => '',
                        'MC7' => 'COLLEGNO (TO)',
                        'MTI' => 'MONCALIERI (TO)',
			'MTS' => 'SAN MAURO TORINESE (TO)',
			'MOA' => 'ORBASSANO (TO)',
			'MS4' => 'SERRAVALLE (AL)',
			'MCC' => 'CORSICO (MI)',
			'MOG' => 'OLGIATE OLONA (VA)',
			'MGO' => 'SAN GIULIANO MILANESE (MI)',
			'MGV' => 'SESTO SAN GIOVANNI (MI)',
			'MRJ' => 'RESCALDINA (MI)',
			'MCX' => 'CURNO (BG)',
			'MOL' => 'BOLOGNA (BO)',
			'MFE' => 'FERRARA (FE)',
			'MUD' => 'UDINE (UD)',
			'MM8' => 'MESTRE (VE)',
			'MDQ' => 'PADOVA (PD)',
			'MVZ' => 'VICENZA (VI)',
			'MRF' => 'ROMA FIUMICINO (Roma)',
			'MCW' => 'ROMA CASILINA (Roma)',
			'MFI' => 'ROMA TORRESINA (Roma)',
			'MRM' => 'ROMA ROMANINA (Roma)',
			'MDV' => 'VITERBO (VT)');
	                        
        if ($dec) return decodifica_valore($valori,$valore);
         
        lista_gen_mag($name,$valore,$class,$refresh,$valori,$disabled);
     }

  function dec_tipi_sezionali($valore) {
      return lista_tipi_sezionali(null,$valore,null,null,true);
  }
  function lista_tipi_sezionali($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'V' => 'Vendita',
                        'A' => 'Acquisto');
        
        if ($dec) return decodifica_valore($valori,$valore);
                                
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     

  function dec_tipologia_iva($valore) {
      return lista_tipologia_iva(null,$valore,null,null,true);
  }
  function lista_tipologia_iva($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Imponibile',
                        'E' => 'Esente',
                        'N' => 'Non Soggetto',
                        'X' => 'Non Imponibile',
                        '7' => '74 Ter');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

  function dec_tipo_decorrenza($valore) {
      return lista_tipo_decorrenza(null,$valore,null,null,true);
  }
  function lista_tipo_decorrenza($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'D' => 'Data Fattura',
                        'F' => 'D.F. Fine Mese',
                        'G' => 'Giorno Fisso');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

    function lista_box($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(  ''=> '',
                        '11'=> '1-1',
                        '12' => '1-2',
                        '21' => '2-1',
                        '22' => '2-2',
                        '31' => '3-1',
                        '32' => '3-2',
                        '41' => '4-1',
                        '42' => '4-2',
                        '51' => '5-1',
                        '61' => '6-1',
                        '71' => '7-1',  
                        '81' => '8-1');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  



  function dec_tipo_pagamento($valore) {
      return lista_tipo_pagamento(null,$valore,null,null,true);
  }
  function lista_tipo_pagamento($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'D' => 'Rimessa Diretta',
                        'B' => 'Bonifico',
                        'R' => 'RiBa',
                        'A' => 'Assegno');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }       

  function lista_numero_rate($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 9) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }          
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_vendita_rimborso($valore) {
      return lista_vendita_rimborso(null,$valore,null,null,true);
  }
  function lista_vendita_rimborso($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'V' => 'Vendita',
                        'R' => 'Rimborso');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }   

  function dec_tipo_pratica($valore) {
      return lista_tipo_pratica(null,$valore,null,null,true);
  }     
  function lista_tipo_pratica($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Intermediazione',
                        'P' => 'Propria');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

  function lista_segno($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        '+' => '+',
                        '-' => '-');
        lista_gen($name,$valore,$class,$refresh,$valori);
     }       

  function dec_tipo_operazione($valore) {
      return lista_tipo_operazione(null,$valore,null,null,true);
  }    
  function lista_tipo_operazione($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Incasso',
                        'S' => 'Saldo');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }         

  function lista_programmi($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        'Air Spent by Month' => 'Air Spent by Month',
                        'Estrazione Dati' => 'Estrazione Dati',
                        'Top City Pairs' => 'Top City Pairs',
                        'Top Destinations' => 'Top Destinations',
                        'Airline Ticket Summary' => 'Airline Ticket Summary'                       
                        );                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_tipifile($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        'csv' => 'Sequenziali',
                        'pdf' => 'PDF',                    
                        );
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     


  function lista_utenti($name,$valore,$class='',$refresh='')
    {
        $valori = array('T'=> 'Tutti',
                        'U' => 'Utenti',
                        'R' => 'Richieste',                    
                        );
        lista_gen($name,$valore,$class,$refresh,$valori);
     }



  function dec_sn($valore) {
      return lista_sn(null,$valore,null,null,true);
  }  
  function lista_sn($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('S' => 'Si',
                        'N' => 'No');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }
     

  function dec_tipomenu($valore) {
      return lista_sn(null,$valore,null,null,true);
  }  
  function lista_tipomenu($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('0' => 'Men&ugrave;',
                        '1' => 'Funzioni');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }
     

  function dec_01($valore) {
      return lista_01(null,$valore,null,null,true);
  }  
  function lista_01($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('1' => 'Si',
                        '0' => 'No');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }    
  function dec_MF($valore) {
      return lista_MF(null,$valore,null,null,true);
  }  
  function lista_MF($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(' ' => '',
                        'M' => 'Maschio',
                        'F' => 'Femmina');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }       

  function dec_exchange($valore) {
      return lista_exchange(null,$valore,null,null,true);
  } 
  function lista_exchange($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('' => '',
                        'N' => 'No', 
                        'S' => 'Si');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }

  function dec_continenti($valore) {
      return lista_continenti(null,$valore,null,null,false,true);
  } 
  function lista_continenti($name,$valore,$class='',$refresh='',$selezione = false,$dec=false)
    { 
        $valori = array(' ' => ' ',
                        'EU' => 'Europa',
                        'AF' => 'Africa',
                        'AS' => 'Asia',
                        'NA' => 'Nord America',
                        'SA' => 'Sud America',
                        'OC' => 'Oceania'
                        );                        
        if (!$selezione) unset($valori[' ']);
        if ($dec) return decodifica_valore($valori,$valore);        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }     

  function lista_ore($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 23) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_order_by($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 9) {
             $valori[$i] = $i;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     

  function lista_minuti($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 59) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_durata($valore) {
      return lista_durata(null,$valore,null,null,true);
  }
  function lista_durata($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('In giornata' => 'In giornata',
                        'Giorno Successivo' => 'Giorno Successivo',
                        'Due giorni dopo' => 'Due giorni dopo',
                        'non disponibile' => 'non disponibile',
                        '' => ' ');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_int($valore) {
      return lista_int(null,$valore,null,null,true);
  }
  function lista_int($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('International' => 'International',
                        'Domestic' => 'Domestic',
                        'non disponibile' => 'non disponibile',
                        '' => ' ');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_tipo_data($valore) {
      return lista_tipo_data(null,$valore,null,null,true);
  }
  function lista_tipo_data($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('V' => 'Vendita',
                        'P' => 'Partenza'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }      

  function dec_tipo_volo($valore) {
      return lista_tipo_volo(null,$valore,null,null,true);
  }
  function lista_tipo_volo($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('IN' => 'Internazionale',
                        'IC' => 'Intercontinentale',
                        'DO' => 'Domestic',
                        'T' => 'Tutti',
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     } 

  function dec_tipo_scalo($valore) {
      return lista_tipo_scalo(null,$valore,null,null,true);
  }
  function lista_tipo_scalo($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('A' => 'Arrivo',
                        'P' => 'Partenza',
                        'T' => 'Tutto'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }  
     
  function lista_valore_perc($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('P' => 'Percentuale','V' => 'Valore');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  
     
  function lista_gen($name,$valore,$class='',$refresh='',$valori)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        echo "\t <select name=\"".$name."\" id=\"".$name."\" class=\"$class\" $refresh>\n";
        foreach ($valori as $key => $value) {
              $selected = "";
              if ($key == $valore ) $selected = "selected";
              echo "\t\t <option value=\"$key\" $selected>$value</option>\n";
        } 
        echo "\t </select>\n";        
     }    

       function lista_gen_mag($name,$valore,$class='',$refresh='',$valori,$disabled)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        echo "\t <select name=\"".$name."\" id=\"".$name."\" class=\"$class\" $refresh $disabled>\n";
        foreach ($valori as $key => $value) {
              $selected = "";
              if ($key == $valore ) $selected = "selected";
              echo "\t\t <option value=\"$key\" $selected>$key - $value</option>\n";
        } 
        echo "\t </select>\n";        
     }    

  function filtro_iniziale ( $pagina,$start, $ordinamento,$numeri = true,$campo = null) {
       echo "<table><tr><td class=\"px\" height=\"3\"></td></tr>";
       
       //if ($start == "%") echo "(attuale &nbsp;Tutti&nbsp;)";
       //elseif ($start != "@") echo "(attuale &nbsp;$start&nbsp;)";
       
       echo "<tr><td>";
       foreach (range('A', 'Z') as $l) {
              
              $val = $l;
              if ($l == $start) $val = "<b>$l</b>";        
       
           echo ("<a href=\"".$pagina."start=$l&order=$ordinamento\">$val</a>&nbsp;&nbsp;");      
       }
       if ($numeri ) {
         for ($i=0;$i<10;$i++) {
             $val = $i;
             if (((String)($i)) == $start) $val = "<b>$i</b>"; 
             echo ("<a href=\"".$pagina."start=$i&order=$ordinamento\">$val </a>&nbsp;&nbsp;");
         }
       }
      
       $val = "Tutti";
       if ($start == "%" || $start == "@"  ) $val = "<b>Tutti</b>";   
       echo ("<a href=\"".$pagina."start=%&order=$ordinamento\">$val</a>&nbsp;&nbsp;");
           
       echo "</td></tr><tr><td class=\"px\" height=\"15\"></td></tr></table>";
  }

  function filtro_ordimanento ( $pagina, $start,$ordinamento,$campo1="Codice",$campo2="Descrizione") {
       echo "<table><tr><td class=\"px\" height=\"5\"></td></tr><tr><td>";
       echo "Seleziona Ordinamento (attuale &nbsp;$ordinamento&nbsp;):</td></tr><tr><td>";
       
       echo ("<a href=\"".$pagina."start=$start&order=codice\">Codice</a>&nbsp;&nbsp;");
       echo ("<a href=\"".$pagina."start=$start&order=descrizione\">Descrizione</a>&nbsp;&nbsp;");      
       echo "</td></tr><tr><td class=\"px\" height=\"15\"></td></tr></table>";
  }

     
  function calendario_js() {
    echo "
    <script type=\"text/javascript\" src=\"../../Html/CalendarPopup.js\"></script>
    <script type=\"text/javascript\">
    var cal = new CalendarPopup();
    var calName = '';
    function setMultipleValues(y,m,d) {
         document.getElementById(calName+'_Y').value=y;
         document.getElementById(calName+'_M').selectedIndex=m*1-1;
         document.getElementById(calName+'_D').selectedIndex=d*1-1;
    }
    function setDate(name) {
      calName = name.toString();
      var year = document.getElementById(calName+'_Y').value.toString();
      var month = document.getElementById(calName+'_M').value.toString();
      var day = document.getElementById(calName+'_D').value.toString();
      var mdy = month+'/'+day+'/'+year;
      cal.setReturnFunction('setMultipleValues');
      cal.showCalendar('anchor', mdy);
    }
    </script>
    ";  
  }
  
  function editor_js() {
    global $complete_folder_image;  
   echo" <!-- TinyMCE -->
    <script type=\"text/javascript\" src=\"../../Html/tiny_mce/tinymce.min.js\"></script>
    <script type=\"text/javascript\">

tinymce.init({
    selector: \"textarea.ed\",
    theme: \"modern\",
    document_base_url: \"".$complete_folder_image."/\" ,
    convert_urls: false,
    plugins: [
        \"advlist autolink lists link image jbimages charmap print preview hr anchor pagebreak\",
        \"searchreplace wordcount visualblocks visualchars code fullscreen\",
        \"insertdatetime media nonbreaking save table contextmenu directionality\",
        \"emoticons template paste textcolor colorpicker textpattern \"
    ],
    toolbar1: \"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages\",
    toolbar2: \"print preview media | forecolor backcolor emoticons\",
    image_advtab: true,
    relative_urls: false,
      force_br_newlines : true,
        force_p_newlines : false,

    extended_valid_elements : \"iframe[src|width|height|name|align]\",

     style_formats: [
          {title: \"Headers\", items: [
                  {title: \"Header 1\", format: \"h1\"},
                  {title: \"Header 2\", format: \"h2\"},
                  {title: \"Header 3\", format: \"h3\"},
                  {title: \"Header 4\", format: \"h4\"},
                  {title: \"Header 5\", format: \"h5\"},
                  {title: \"Header 6\", format: \"h6\"}
              ]},
              {title: \"Inline\", items: [
                  {title: \"Bold\", icon: \"bold\", format: \"bold\"},
                  {title: \"Italic\", icon: \"italic\", format: \"italic\"},
                  {title: \"Underline\", icon: \"underline\", format: \"underline\"},
                  {title: \"Strikethrough\", icon: \"strikethrough\", format: \"strikethrough\"},
                  {title: \"Superscript\", icon: \"superscript\", format: \"superscript\"},
                  {title: \"Subscript\", icon: \"subscript\", format: \"subscript\"},
                  {title: \"Code\", icon: \"code\", format: \"code\"}
              ]},
              {title: \"Blocks\", items: [
                  {title: \"Paragraph\", format: \"p\"},
                  {title: \"Blockquote\", format: \"blockquote\"},
                  {title: \"Div\", format: \"div\"},
                  {title: \"Pre\", format: \"pre\"}
              ]},
              {title: \"Alignment\", items: [
                  {title: \"Left\", icon: \"alignleft\", format: \"alignleft\"},
                  {title: \"Center\", icon: \"aligncenter\", format: \"aligncenter\"},
                  {title: \"Right\", icon: \"alignright\", format: \"alignright\"},
                  {title: \"Justify\", icon: \"alignjustify\", format: \"alignjustify\"}
              ]},
     
        {
            title: 'Image Left',
            selector: 'img',
            styles: {
                'float': 'left', 
                'margin': '0 10px 0 10px'
            }
         },
         {
             title: 'Image Right',
             selector: 'img', 
             styles: {
                 'float': 'right', 
                 'margin': '0 0 10px 10px'
             }
         }
    ]    
});
   </script>
    <!-- /TinyMCE -->";
  }
  
  function ajaxcall() {
      echo "<!--script type=\"text/javascript\" src=\"../../Html/ajax.js\"></script-->";  
  }
  
  function datepickersetting() {
  
        return "numberOfMonths: 1,
        			showButtonPanel: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              dateFormat: 'dd/mm/yy',
              changeYear: true,
              changeMonth: true,
              closeText:'Fatto',
              currentText:'Oggi',
              dayNames: ['Domenica', 'Lunedi', 'Martedi', 'Mercoledi', 'Giovedi', 'Venerdi', 'Sabato'],
              dayNamesMin: ['Do', 'Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa'],
              dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
              monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
              monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic']"; 
  }
  
  function datepicker($nome) {
      echo "       <script>
              	$(function() {
              		$( \"#".$nome."\" ).datepicker({".datepickersetting().",
                  beforeShow: function (textbox, instance) {
                                  instance.dpDiv.css({
                                         marginTop: (-textbox.offsetHeight) + 'px',
                                         marginLeft: textbox.offsetWidth + 'px'
                                                     });
                                          },                  
              		});
              	});
              </script>";  
  }


  function addclearDatepicker() {
    echo "       <script> $(function(){
                	//wrap up the redraw function with our new shiz
                	var dpFunc = $.datepicker._generateHTML; //record the original
                	$.datepicker._generateHTML = function(inst){
                		var thishtml = $( dpFunc.call($.datepicker, inst) ); //call the original
                		
                		thishtml = $('<div />').append(thishtml); //add a wrapper div for jQuery context
                		
                		//locate the button panel and add our button - with a custom css class.
                		$('.ui-datepicker-buttonpane', thishtml).append(
                			$('<button class=\"ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all\" \>Pulisci</button>'
                			).click(function(){
                				inst.input.attr('value', '');         
                        inst.input.blur();
                				inst.input.datepicker('hide');
                			})
                		);
                		
                		thishtml = thishtml.children(); //remove the wrapper div
                		
                		return thishtml; //assume okay to return a jQuery
                	};
                }); </script>";   
  }

  function datepickerFromTo($nome,$nome1) {
      echo "       <script>
             	$(function() {
              	 var dates =	$( \"#".$nome.",#".$nome1."\" ).datepicker({defaultDate: \"+1w\",
                          ".datepickersetting()."
                    			, beforeShow: function (input, instance) {
                                  instance.dpDiv.css({
                                         marginTop: (-input.offsetHeight) + 'px',
                                         marginLeft: input.offsetWidth + 'px'
                                                     });                          
                                  return {
                                                    minDate: (input.id == \"".$nome."\" ? new Date(2008, 12 - 1, 1) : null),
                                                    minDate: (input.id == \"".$nome1."\" ? $(\"#".$nome."\").datepicker(\"getDate\") : null), 
                                                    maxDate: (input.id == \"".$nome."\" ? $(\"#".$nome1."\").datepicker(\"getDate\") : null),
                                                    defaultDate: (input.id == \"".$nome1."\" ? $(\"#".$nome."\").datepicker(\"getDate\") : null)
                                       };                                           
                          }
              		});
              	});
              </script>";  
  }  
?>
