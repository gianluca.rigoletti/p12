<?php

  function get_lingue() {
      $sql = db_query_vis("lingue","ID");
      $lingue = array();
      while ($cur_rec = mysql_fetch_assoc($sql)) {
          $lingue[] = $cur_rec;
      } 
      
      return $lingue;
  }
  
  function editor_lingua($elenco_campi,$suffisso = null) {
  
       $lingue = get_lingue();
       
       echo "<div id=\"ges_lingue$suffisso\"> \n<ul>\n";
        
       for ($i=0;$i<count($lingue);$i++) {
           echo "<li><a href=\"#tab_".$lingue[$i]["CODICE"].$suffisso."\">".$lingue[$i]["DESCRIZIONE"]."</a></li>\n";
       }
       echo "</ul>\n";
             
       for ($i=0;$i<count($lingue);$i++) {               
            $lingua = $lingue[$i];
           echo "<div id=\"tab_".$lingua["CODICE"].$suffisso."\">\n <table width=\"100%\">";
      
           for ($x=0;$x<count($elenco_campi);$x++) {
                $campo =  $elenco_campi[$x];

                $nome = $campo[1]."[".$lingua['ID']."]";
                $valore = "";
                
                if (!is_array($campo[2])) {       
                   if ($i == 0)  $valore  = $campo[2];
                } else {           
                   if (isset($campo[2][$lingua['ID']])) $valore  = $campo[2][$lingua['ID']];
                }
                
                $label = "label :";
                if (isset($campo[3] )) $label  = $campo[3] ;
                
                $class = "";
                if (isset($campo[4] )) $class  = " class=\"".$campo[4]."\"";                
                
                if ($campo[0] == "TEXT") {
                    echo "<tr><td class=\"label\" width=\"25%\"> ".$label." </td>
                     <td width=\"75%\"> ";
                    echo "<input type=\"text\" $class name=\"".$nome."\" id=\"$nome\" value=\"$valore\"   size=\"60\">";
                    echo "</td></tr>";
                }

                if ($campo[0] == "TEXTAREA") {
                   
                    echo "<tr><td colspan=\"2\" class=\"label\"> ".$label ." </td></tr>";
                    echo "<tr><td colspan=\"2\">";
                    echo "<textarea $class  name=\"".$nome."\" id=\"$nome\" style=\"width:100%\">$valore</textarea>";
                    echo "</td></tr>";
                }                
                
          
           }   
           
           echo "</table>\n</div>";
       }
      
      echo "\n</div>"; // chiude div ges_lingue
      
      echo "\n<script>
            $(function() {
              $( \"#ges_lingue$suffisso\" ).tabs();
            });
           </script>";
  }
  

  function unser($data)
  {
  	$data_arr = array();
  	if(!is_array($data)) {
        $test = @unserialize(base64_decode($data));
        if ($test !== false) {
           return  unserialize(base64_decode($data));
         } else {
            return $data;
         }
   //	return unserialize($data);
  	} else {          
  		foreach ($data as $value) { 
  			array_push($data_arr, unserialize(base64_decode($value)));
  		}           
  		return $data_arr;
  	}
  }

  function inser($data)
  {
  	$data = base64_encode(serialize($data));
    //$data = (serialize($data));
  	return $data;
  }
  
  function descrizione_singola($campo) {
       return contenuto_lingua($campo,false);    
  }
  
  function contenuto_lingua($campo,$lingua = true) {
         global $lingua_default;
       $valore = unser($campo);
       if (!is_array($valore)) { // nn � mai stato tradotto quindi prendo sempre quello che c'�       
           return $valore;
       }  else {
        
 
          if ($lingua && isset($valore[$lingua_default]) && !db_is_null($valore[$lingua_default])) {

              return $valore[$lingua_default];          
          } else {
             foreach ($valore as $k => $v) {
              if (!db_is_null($v)) return $v; // torno il primo valorizzato qualunque lingua sia           
             }
          }
       } 
       return $campo;  
  
  }
  
  function disegna_bandiere() {
  
        $lingue = get_lingue();
        echo "<div class=\"lingue\">";
        for ($i=0;$i<count($lingue);$i++) {
         echo "
         <div id=\"".$lingue[$i]['CODICE']."-lang\" class=\"bandiera\" style=\"float:left\">
               	<a href=\"javascript:lingua_form(".$lingue[$i]['ID'].",'".$lingue[$i]['CODICE']."')\" rel=\"nofollow\"><img title=\"".$lingue[$i]['DESCRIZIONE']."\" alt=\"\" src=\"Immagini/".$lingue[$i]['ICONA']."\" /></a> 
                </div>";        
        }
   
        echo "</div>";

        
        
        echo  '<form name="form_lingue" id="form_lingue" target="" action="" method="post">
                 <input type="hidden" id="lingua" name="lingua" value=""/>
                 <input type="hidden" id="cod_lingua" name="cod_lingua" value=""/>
               </form>
               
               <script>
                  function lingua_form(id,cod) {
                     $("#lingua").val(id);
                     $("#cod_lingua").val(cod);
                     $("#form_lingue").submit();
                  }                
               </script>';  
  }
  
   function get_label_sito($label = null ) {
   
       if (!isset($_SESSION['label']))  {
             $ris = cerca_label_sito  (); 
              $array_label = array();
              while ($r =mysql_fetch_assoc($ris) ) {
                $array_label[$r['CODICE']] = unser($r['DESCRIZIONE']);
              }
              $_SESSION['label'] = $array_label;
       }
       elseif (!isset($_SESSION['label'][$label])) {
             $array_label = $_SESSION['label'];
             $ris = cerca_label_sito($label);
             while ($r =mysql_fetch_assoc($ris) ) {
               $array_label[$r['CODICE']] = unser($r['DESCRIZIONE']);
             }
             $_SESSION['label'] = $array_label;
       } 
       
       if (!isset($_SESSION['label'][$label])) {
            return  $label;
       }
       return  $_SESSION['label'][$label][$_SESSION['lingua'] ];
   }


   function cerca_label_sito($label = null) {
       if ($label == null)
          $ris = db_query_generale("label","1=1",'id');
       else 
          $ris = db_query_generale("label","CODICE = '".$label."'",'id');

       return $ris;          
   }
   
   function inizializzaLingua($post,$get) {
       global $lingua_default;
       global $cod_lingua_default,$facebook_lingua;
      
       $lingua_default =1;
       $cod_lingua_default = "it";
       $refresh = false;
       
       if (isset($get['l'])) { $post['lingua'] = $get['l']; unset($get['l']); }
       if (isset($get['c'])) { $post['cod_lingua'] = $get['c'];  unset($get['c']); }
       
       $q = "";
       foreach ($get as $k => $v ) {
           $q= $k."=".$v."&";
       } 
        
       if (isset($post['lingua'])) {$refresh = true; $_SESSION['lingua'] =  $post['lingua']; unset($post['lingua']); }
       if (isset($post['cod_lingua'])) { $_SESSION['cod_lingua'] =  $post['cod_lingua'];  unset($post['cod_lingua']); }
       
       if (!isset($_SESSION['lingua']) ) {
           $_SESSION['lingua'] = $lingua_default;
           $_SESSION['cod_lingua'] = $cod_lingua_default;
       }
       $lingua_default =  $_SESSION['lingua'];
       $cod_lingua_default = $_SESSION['cod_lingua'];
       
       $facebook_lingua = "en_US";
       if ($_SESSION['cod_lingua'] == "IT") $facebook_lingua = "it_IT";    
        
        
       if ($refresh) {
       header('Location:'.$_SERVER['PHP_SELF'].'?'.$q);//$_SERVER['QUERY_STRING']);
       die;
       }
       return $post;
  }


?>
