<?php

class get_sito
{

   function sito() {
   
     if (isset($_SESSION['sito']) && isset($_SESSION['sito']['ID'])) {
      //  return  $_SESSION['sito'];
     }
   
     $server =  $_SERVER['SERVER_NAME'];
     
     $rec = db_query_generale("siti_mapping","upper(hostname) = upper('".$server."')",null);
     if (!$rec) return false;
     
     $mapping = mysql_fetch_assoc($rec);
     
     if (isset($mapping['IDSito']) && $mapping['IDSito'] != 0 && $mapping['IDSito'] != null ) {
        $rec = db_query_mod("siti",$mapping['IDSito']);
        if (!$rec) return false;
        
        $sito = mysql_fetch_assoc($rec);
        if (isset($sito['ID']) && $sito['ID'] != 0 && $sito['ID'] != null ) {
            $_SESSION['sito'] = $sito;
            return $sito;
        }else return false;
           
     }  else {
        return false;
     }
  }
  
  function sendMail($mittente,$destinatario,$oggetto,$testo) {
  
     $sito = $this->sito();
     $mittente  = $sito['smtpuser'];
     if (db_is_null($sito['smtpserver']) ) {

         $header = "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html; charset=utf-8\r\n" ;           
         $header .= "From: <".$mittente."> \r\n";
         //$testo .= "senza smtp da dentro getsito!!!";
         $header .= "Bcc: diego.cesana@yperesia.it\r\n";
         return mail($destinatario,$oggetto,$testo,$header);        
     }     
     
     
     //$mittente  = $sito['smtpuser'];
     require_once('phpmailer/class.phpmailer.php');
     $mail             = new PHPMailer();



     $mail->AddAddress($destinatario, "");
     
     $mail->AddBcc('diego.cesana@yperesia.it', "");
     $mail->AddBcc('andrea.zordan@yperesia.it', "");
     $mail->Subject    = $oggetto;
     $mail->AltBody    = ""; // optional, comment out and test
     $mail->Body = $testo;
     $mail->IsHTML(true);   
     
   //  $mail->AddReplyTo($mittente,'');
     $mail->SetFrom($mittente, '');
     $mail ->Host = $sito['smtpserver'];
     $mail ->Port = 25;
     $mail ->Mailer = 'smtp';
     $mail ->SMTPSecure = '';
     $mail->SMTPDebug  = 1;
     if (!db_is_null($sito['smtppwd']) ) {
         $mail ->SMTPAuth = true;
         $mail ->Username = $sito['smtpuser'];
         $mail ->Password = $sito['smtppwd']; 
     }   else {
         $mail ->SMTPAuth = false;
         echo "auto-false";
         //$mail ->Username = $sito['smtpuser'];
         //$mail ->Password = $sito['smtppwd']; 
     }
     $mail ->IsSMTP();        
     
    if(!$mail->Send()) {
       return false;
    }      
    
    return true;
     
  
  }
  
  function stat() {

     $sito = $this->sito();
     if (!$sito )return false;   
     
     $ip = $_SERVER['REMOTE_ADDR'];
     $pag = $_SERVER['SCRIPT_NAME'];
     $utente = "";
     if (isset($_SESSION['login_eseguito']) && $_SESSION['login_eseguito'] == "ok") {
        $utente = $_SESSION['DATI_UTENTE']['email'];
     }
     $query_string = "";
     if ($_REQUEST) {
       $kv = array();
       foreach ($_REQUEST as $key => $value) {
       
         if ($key == "PHPSESSID") continue;
         $kv[] = "$key=$value";
       }
       $query_string = join("&", $kv);
     } 
     
     $sql = "insert into log (ipaddress,utente,evento,descrizione,idsito) 
                  values ( '".$ip."' , '".$utente."', '".$pag."', '".$query_string."', ".$sito['ID']." )";
              
	   $result = mysql_query($sql);
     if (! $result ) return false;                 
     return true;
  }  
  
  function naviga($funzione) {
  
  }
  
  function linkto($path) {
  
  }
  
  function permalink($tipo,$destinazione) {
      /* tipo 0 = pagina
              1 = funzione
         */
         
      $sito = $this->sito();
      if (!$sito )return false;   

      if ($sito['permalink'] == 1) {
          if ($tipo == 0) {
              
              $ris = db_query_mod("pagine",$destinazione);
              $pagina = mysql_fetch_assoc($ris); 
              if (!$pagina) return ("page.php?id=".$destinazione);
              
                            
          
              if (!db_is_null($pagina['PERMALINK'])) {
                  return ("pagina-".$pagina['PERMALINK']); 
              } else {
                  return ("page.php?id=".$destinazione); 
              }
          } else if ($tipo == 1) {
               return ("do-".$destinazione); 
          }      
      } else {
         if ($tipo == 0) {
             return ("page.php?id=".$destinazione); 
          } else if ($tipo == 1) {
             return ("index.php?f=".$destinazione); 
          }
      
      }
  
  }


}

?>