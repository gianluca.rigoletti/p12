<?php
  $classeBody = "";
  $title      = "Risposte Televoto";
  $no_header  = true;
  include "header.php";

  if (!isset($_GET['id']) || db_is_null($_GET['id'])) {
    // Nel caso qualcosa andasse storto ritorna alla riabilitazione domande
    echo "
    <script> window.location.href = 'Admin/Base/abilitazione_domande';</script>
    ";
  }

  function calcolaPercentualePrecaricati($risposta) {
    $sql_valori_precaricati = "SELECT (risposta_1_a + risposta_1_b + risposta_1_c) as risposta1,
    (risposta_2_a + risposta_2_b + risposta_2_c) as risposta2,
    (risposta_3_a + risposta_3_b + risposta_3_c) as risposta3,
    (risposta_4_a + risposta_4_b + risposta_4_c) as risposta4,
    (risposta_1_a + risposta_1_b + risposta_1_c)+
    (risposta_2_a + risposta_2_b + risposta_2_c)+
    (risposta_3_a + risposta_3_b + risposta_3_c)+
    (risposta_4_a + risposta_4_b + risposta_4_c) as totale
    FROM televoto
    WHERE ID = ".$_GET['id'];

    $query_valori_precaricati  = mysql_query($sql_valori_precaricati);
    $res_valori_precaricati  = mysql_fetch_assoc($query_valori_precaricati);
    if (!$query_valori_precaricati) {
      echo "<script>window.location.href = 'Admin/Base/abilitazione_domande.php';</scrip>";
    }
    $percentuale = round($res_valori_precaricati['risposta'.$risposta] / $res_valori_precaricati['totale'], 4) * 100;
    return $percentuale;
  }

  /*
  Funzione che restituisce la percentuale totale data la domanda
  */
  function calcolaPercentualeTotale($risposta) {
    $sql_risposta = "SELECT COUNT(risposte_televoto.id_domanda) as num
    FROM risposte_televoto
    WHERE risposte_televoto.id_domanda = ".$_GET['id']."
    AND risposte_televoto.risposta =".$risposta;

    $sql_totale = "SELECT COUNT(risposte_televoto.id_domanda) as num
    FROM risposte_televoto
    WHERE risposte_televoto.id_domanda = ".$_GET['id'];

    $query_risposta = mysql_query($sql_risposta);
    $res_risposta = mysql_fetch_assoc($query_risposta);

    $query_totale = mysql_query($sql_totale);
    $res_totale = mysql_fetch_assoc($query_totale);
    if (!$query_totale || !$query_risposta) {
      echo "<script>window.location.href = 'Admin/Base/abilitazione_domande.php';</scrip>";
    }
    $percentuale = round($res_risposta['num'] / $res_totale['num'], 4) * 100;
    return $percentuale;
  }

  /*
  Se la categoria e' impostata a 0 allora restituisce la percentuale totale,
  altrimenti restituisce la percentuale relativa alla categoria 1,2 o 3
  */
  function calcolaPercentualeCategoria($risposta, $categoria) {

    // Query che raggruppa per categoria le risposte alle varie domande e
    // restituisce il totale sotto l'alias somma
    $sql_categoria = "SELECT COUNT(user.ID) as num, user.categoria as categoria
    FROM risposte_televoto, user
    WHERE user.ID = risposte_televoto.id_utente
    AND risposte_televoto.id_domanda = ".$_GET['id']."
    AND categoria = $categoria
    AND risposte_televoto.risposta = ".$risposta;

    $sql_totale = "SELECT COUNT(user.ID) as num, user.categoria as categoria
    FROM risposte_televoto, user
    WHERE user.ID = risposte_televoto.id_utente
    AND risposte_televoto.id_domanda = ".$_GET['id'];

    $query_categoria = mysql_query($sql_categoria);
    $res_categoria   = mysql_fetch_assoc($query_categoria);
    $query_totale    = mysql_query($sql_totale);
    $res_totale      = mysql_fetch_assoc($query_totale);
    $percentuale     = round($res_categoria['num'] / $res_totale['num'], 4) * 100;
    return $percentuale;
  };

?>

<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear2"></div>
                <div class="divisore"></div>
                <div class="clear2"></div>

                <div>
                <h1>Lorem Ipsum dolor Amet ?</h1>
                </div>

                <div class="clear2"></div>
                <div class="divisore"></div>
                <div class="clear2"></div>

                <div class="navigazione">
                <a href="#" class="votanti"><img src="immagini/framework/percentuali/bot-1.png" width="20px"></a>
                <a href="#" class="anno-precedente"><img src="immagini/framework/percentuali/bot-2.png" width="20px"></a>
                </div>

<!-- prima risposta ---->

                <div class="uno">

                <div id="domanda">
                <h2>Questa è la risposta numero 1</h2>
                </div>

                <div id="percentuali">

                <div class="colonna-1">

                <div class="numero">
                <a href="#" onclick="uno()">
                <img src="immagini/framework/percentuali/1.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="uno" class="percentualerossa" style="width:0%"></div>
                </div>

                </div>

                <div class="colonna-2">

                <div id="box-votanti">

                <div class="nomi-votanti">
                <a href="#" onclick="investitori1()">
                <h3>INVESTITORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="investitori1" class="percentualerossa-vot" style="width:0%"></div>
                </div>

                <div class="clear2"></div>
                <div class="divisore-votanti"></div>
                <div class="clear2"></div>

                <div class="nomi-votanti">
                <a href="#" onclick="gestori1()">
                <h3>GESTORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="gestori1" class="percentualeblu-vot" style="width:0%"></div>
                </div>

                </div>

                </div>


                <div class="colonna-3">

                <div id="box-anno-precedente">

                <div class="numero">
                <a href="#" onclick="unogrigio()">
                <img src="immagini/framework/percentuali/1grigio.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="unogrigio" class="percentuale-grigia-ch" style="width:0%"></div>
                </div>
               </div>

                </div>

                </div>

                </div>

<!----- fine prima risposta ---->

                <div class="clear2"></div>
                <div class="divisore"></div>
                <div class="clear2"></div>

<!-- seconda risposta ---->

                <div class="due">

                <div id="domanda">
                <h2>Questa è la risposta numero 2</h2>
                </div>

                <div id="percentuali">

                <div class="colonna-1">

                <div class="numero">
                <a href="#" onclick="due()">
                <img src="immagini/framework/percentuali/2.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="due" class="percentualeblu" style="width:0%"></div>
                </div>

                </div>

                <div class="colonna-2">

                <div id="box-votanti2">

                <div class="nomi-votanti">
                <a href="#" onclick="investitori2()">
                <h3>INVESTITORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="investitori2" class="percentualerossa-vot" style="width:0%"></div>
                </div>

                <div class="clear2"></div>
                <div class="divisore-votanti"></div>
                <div class="clear2"></div>

                <div class="nomi-votanti">
                <a href="#" onclick="gestori2()">
                <h3>GESTORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="gestori2" class="percentualeblu-vot" style="width:0%"></div>
                </div>

                </div>

                </div>

                <div class="colonna-3">

                <div id="box-anno-precedente2">

                <div class="numero">
                <a href="#" onclick="duegrigio()">
                <img src="immagini/framework/percentuali/2grigio.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="duegrigio" class="percentuale-grigia-sc" style="width:0%"></div>
                </div>

                </div>

                </div>

                </div>

                </div>

<!----- fine seconda risposta ---->

                <div class="clear2"></div>
                <div class="divisore"></div>
                <div class="clear2"></div>


<!-- terza risposta ---->

                <div class="tre">

                <div id="domanda">
                <h2>Questa è la risposta numero 3</h2>
                </div>

               <div id="percentuali">

                <div class="colonna-1">

                <div class="numero">
                <a href="#" onclick="tre()">
                <img src="immagini/framework/percentuali/3.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="tre" class="percentualerossa" style="width:0%"></div>
                </div>

                </div>

                <div class="colonna-2">

                <div id="box-votanti3">

                 <div class="nomi-votanti">
                <a href="#" onclick="investitori3()">
                <h3>INVESTITORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="investitori3" class="percentualerossa-vot" style="width:0%"></div>
                </div>

                <div class="clear2"></div>
                <div class="divisore-votanti"></div>
                <div class="clear2"></div>

                <div class="nomi-votanti">
                <a href="#" onclick="gestori3()">
                <h3>GESTORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="gestori3" class="percentualeblu-vot" style="width:0%"></div>
                </div>

                </div>

                </div>

                <div class="colonna-3">

                <div id="box-anno-precedente3">

               <div class="numero">
                <a href="#" onclick="tregrigio()">
                <img src="immagini/framework/percentuali/3grigio.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="tregrigio" class="percentuale-grigia-ch" style="width:0%"></div>
                </div>

                </div>


                </div>

                </div>

                </div>

<!----- fine terza risposta ---->

                <div class="clear2"></div>
                <div class="divisore"></div>
                <div class="clear2"></div>


<!-- quarta risposta ---->

                <div class="quattro">

                <div id="domanda">
                <h2>Questa è la risposta numero 4</h2>
                </div>

                <div id="percentuali">

                <div class="colonna-1">

                <div class="numero">
                <a href="#" onclick="quattro()">
                <img src="immagini/framework/percentuali/4.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="quattro" class="percentualeblu" style="width:0%"></div>
                </div>

                </div>

                <div class="colonna-2">


                <div id="box-votanti4">

                 <div class="nomi-votanti">
                <a href="#" onclick="investitori4()">
                <h3>INVESTITORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="investitori4" class="percentualerossa-vot" style="width:0%"></div>
                </div>

                <div class="clear2"></div>
                <div class="divisore-votanti"></div>
                <div class="clear2"></div>

                <div class="nomi-votanti">
                <a onclick="gestori4()" href="#">
                <h3>GESTORI</h3>
                </a>
                </div>
                <div class="pr-votanti">
                <div id="gestori4" class="percentualeblu-vot" style="width:0%"></div>
                </div>

                </div>


                </div>

                <div class="colonna-3">

                <div id="box-anno-precedente4">

                <div class="numero">
                <a href="#" onclick="quattrogrigio()">
                <img src="immagini/framework/percentuali/4grigio.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuale">
                <div id="quattrogrigio" class="percentuale-grigia-sc" style="width:0%"></div>
                </div>

                </div>


                </div>

                </div>

                </div>

<!----- fine quarta risposta ---->

<!-- primo bottone
				 <div class="uno">

                 <div>
                <div class="titolo">
                <h2>Lorem Ipsum dolor amet dolor amet</h2>
                </div>
                </div>

                <div class="numeri">
                <a onclick="uno()">
                <img src="immagini/framework/percentuali/1.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuali">
                <div id="uno" class="percentualerossa" style="width:0%"></div>
                </div>

                <div class="percentuali">
                <div style=" background:#FF0409; height:20px"></div>
                </div>
                <div class="percentuali">
                <div style=" background:#000000; height:20px"></div>
                </div>

                </div>
<!-- fine primo bottone

                  <div class="clear2"></div>

<!-- secondo bottone
                <div class="due">

                <div class="divisore"></div>

                <div>
                <div class="titolo">
                <h2>Lorem Ipsum dolor amet dolor amet</h2>
                </div>
                </div>


                <div class="numeri">
                <a onclick="due()">
                <img src="immagini/framework/percentuali/2.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuali">
                <div  id="due" class=" percentualeblu" style="width:0%"></div>
                </div>

                <div class="percentuali">
                <div style=" background:#FF0409; height:20px"></div>
                </div>
                <div class="percentuali">
                <div style=" background:#000000; height:20px"></div>
                </div>

                </div>
<!-- fine secondo bottone

                  <div class="clear2"></div>

<!-- terzo bottone
                <div class="tre">

                <div class="divisore"></div>

                <div>
                <div class="titolo">
                <h2>Lorem Ipsum dolor amet dolor amet</h2>
                </div>
                </div>

                <div class="numeri">
                <a onclick="tre()">
                <img src="immagini/framework/percentuali/3.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuali">
                <div id="tre" class=" percentualerossa" style="width:0%"></div>
                </div>

                <div class="percentuali">
                <div style=" background:#FF0409; height:20px"></div>
                </div>
                <div class="percentuali">
                <div style=" background:#000000; height:20px"></div>
                </div>

                </div>
<!-- fine terzo bottone

                  <div class="clear2"></div>

<!-- quarto bottone
                <div class="quattro">

                <div class="divisore"></div>


                <div>
                <div class="titolo">
                <h2>Lorem Ipsum dolor amet dolor amet</h2>
                </div>
                </div>

                <div class="numeri">
                <a onclick="quattro()">
                <img src="immagini/framework/percentuali/4.jpg" width="55px" />
                </a>
                </div>

                <div class="percentuali">
                <div id="quattro" class=" percentualeblu" style="width:0%"></div>
                </div>

                <div class="percentuali">
                <div style=" background:#FF0409; height:20px"></div>
                </div>
                <div class="percentuali">
                <div style=" background:#000000; height:20px"></div>
                </div>

                </div>
<!-- fine quarto bottone

                  <div class="clear2"></div>------>

            </div>

        </div>


<style type="text/css">
body div {
    display: block;
}
.titolo {padding:20px 10px;}

.divisore {
    border-bottom: 3px dotted #0065a4;
    margin-bottom: 20px;
}

.divisore-votanti {
    border-bottom: 3px dotted #0065a4;
}

h1 {
	font-size:40px;
	color:#454545;
	text-align:center !important;
}
h2 {
	font-size:30px;
	color:#000000;
}
h3 {
	line-height:20px;
	margin-bottom:0px;
	}

.snap-content {
    margin-left: auto;
    margin-right: auto;
    width: 1500px;
}

.content {
	padding: 0px !important;
}

.uno, .due, .tre, .quattro { width:100%;}

#titolo, #percentuali { width:100%;}

.uno img, .due img, .tre img, .quattro img { float:right;}

.percentualerossa{
	background:#cd123f;
	height: 79px !important;
}

.percentualerossa-vot {
	background:#cd123f;
	height: 17px;
}

.percentualeblu {
	background:#1f5093;
    height: 79px !important;
}

.percentualeblu-vot {
	background:#1f5093;
	height: 17px;
}


.percentuale-grigia-ch{
	background:#6F6F6F;
	height: 79px !important;
}

.percentuale-grigia-sc{
	background:#595959;
	height: 79px !important;
}

.nomi-votanti {width:140px;float:left;}

.pr-votanti {width:320px;float:left;}

.colonna-1, .colonna-2, .colonna-3 { width:460px; float:left; min-height: 10px;}

.colonna-1 { margin-left:10px; margin-right:10px; }
.colonna-2 { margin-right:10px; }
.colonna-3 { margin-right:10px; }

.numero {
	float: left;
    min-height: 10px;
    width: 10%;
}


.percentuale {
    float: left;
    min-height: 10px;
    width: 90%;
}

#box-votanti, #box-votanti2, #box-votanti3, #box-votanti4, #box-anno-precedente, #box-anno-precedente2, #box-anno-precedente3, #box-anno-precedente4 {display:none;}

.navigazione { float:right; z-index:99999; width:60px;}

.navigazione img { float:left; margin-left:10px;}

</style>


<script>
function uno() {
  var elem = document.getElementById("uno");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeTotale(1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeTotale(1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function unogrigio() {
  var elem = document.getElementById("unogrigio");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualePrecaricati(1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualePrecaricati(1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function due() {
  var elem = document.getElementById("due");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeTotale(2); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeTotale(2); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function duegrigio() {
  var elem = document.getElementById("duegrigio");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualePrecaricati(2) ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualePrecaricati(2); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}


function tre() {
  var elem = document.getElementById("tre");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeTotale(3) ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeTotale(3); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function tregrigio() {
  var elem = document.getElementById("tregrigio");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualePrecaricati(3); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualePrecaricati(3); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}


function quattro() {
  var elem = document.getElementById("quattro");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeTotale(4); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeTotale(4); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function quattrogrigio() {
  var elem = document.getElementById("quattrogrigio");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualePrecaricati(4); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualePrecaricati(4); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function investitori1() {
  var elem = document.getElementById("investitori1");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(1,1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(1,1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function gestori1() {
  var elem = document.getElementById("gestori1");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(1,2); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(1,2) ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function investitori2() {
  var elem = document.getElementById("investitori2");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(2,1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(2,1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function gestori2() {
  var elem = document.getElementById("gestori2");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(2,2); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(2,2); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function investitori3() {
  var elem = document.getElementById("investitori3");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(3,1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(3,1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function gestori3() {
  var elem = document.getElementById("gestori3");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(3,2); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(3,2) ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function investitori4() {
  var elem = document.getElementById("investitori4");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(4,1); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(4,1); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

function gestori4() {
  var elem = document.getElementById("gestori4");
  var width = 0;
  var id = setInterval(frame, <?php echo calcolaPercentualeCategoria(4,2); ?>);
  function frame() {
    if (width >= <?php echo calcolaPercentualeCategoria(4,2); ?>) {
      clearInterval(id);
    } else {
      width++;
      elem.style.width = width + '%';
    }
  }
}

$(document).ready(function() {

            $(".anno-precedente").click(
            function(){
            $("#box-anno-precedente").show("slow");//il box apparirà più lentamente
            });

			$(".anno-precedente").click(
            function(){
            $("#box-anno-precedente2").show("slow");//il box apparirà più lentamente
            });

			$(".anno-precedente").click(
            function(){
            $("#box-anno-precedente3").show("slow");//il box apparirà più lentamente
            });

			$(".anno-precedente").click(
            function(){
            $("#box-anno-precedente4").show("slow");//il box apparirà più lentamente
            });

			$(".anno-precedente").click (
			function(){
			$(".anno-precedente").hide("fast");
			});

});

$(document).ready(function() {

            $(".votanti").click(
            function(){
            $("#box-votanti").show("slow");//il box apparirà più lentamente
            });

			$(".votanti").click(
            function(){
            $("#box-votanti2").show("slow");//il box apparirà più lentamente
            });

			$(".votanti").click(
            function(){
            $("#box-votanti3").show("slow");//il box apparirà più lentamente
            });

			$(".votanti").click(
            function(){
            $("#box-votanti4").show("slow");//il box apparirà più lentamente
            });

			$(".votanti").click (
			function(){
			$(".votanti").hide("fast");
			});

});


</script>
</body>
