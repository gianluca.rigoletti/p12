<?php
  $title = "percorso 17 giugno";
  $no_header = false;
  include "header.php";
?>


<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="two-third-responsive blue">
                <h1>percorso 17 giugno</h1>
                </div>

                <div class="one-third-responsive last-column red">
                <a href="televoto.php">
                <h1>televoto</h1>
                </a>
                </div>


                <div class="clear"></div>

               <div class="list">
               <p>
               <a href="#">
               <strong>08.45</strong><br/>﻿﻿<span>Saluto di benvenuto</span><br/><em>Angelo Tantazzi, Presidente - Prometeia</em>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="#">
               <strong>09.00</strong><br/>﻿﻿﻿﻿<span>Dalla trappola della liquidità all'entropia dei rendimenti?</span><br/>﻿<em>Paolo Onofri, Vicepresidente - Prometeia Advisor Sim</em>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="#">
               <strong>09.30</strong><br/>﻿﻿<span>﻿﻿Asset allocation: "la conquista del west" o "un americano a roma"?</span><br/><em>﻿Lea Zicchino, Partner - Prometeia</em>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="#">
               <strong>10.00</strong><br/>﻿﻿﻿﻿<span>La gestione del rischio nel regno delle banche centrali</span><br/>﻿<em>Paolo Moia, Responsabile Area Asset Management - Banca Profilo</em>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="#">
               <strong>10.30</strong><br/>﻿﻿<span>﻿﻿Coffee break</span>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="ora-10-45.php">
               <strong>10.45</strong><br/>﻿﻿<span>﻿﻿Dall’approccio multisettoriale alla gestione dinamica del rischio nei portafogli reddito fisso</span><br/>﻿<em>Nicola Meotti, Director - Financial Institutions - AB</em>
               </a>
               </p>
               </div>

               <div class="list">
               <p>
               <a href="#">
               <strong>11.15</strong><br/>﻿﻿﻿﻿<span>Profili di rischio/rendimento asimmetrici: La medaglia senza il rovescio</span><br/><em>﻿Lorenzo Gazzoletti, Deputy CEO - Oddo Asset Management</em>
               </a>
               </p>
               </div>


                <div class="clear"></div>




            </div>
           <?php include "footer.php" ?>


        </div>



</body>
