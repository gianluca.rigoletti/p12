<?php
  $classeBody = "";
  $no_header = true;
  $title = "Attesa Televoto";
  include "header.php";

  /*
    se è presente una domanda in stato attivo che l'utente non ha ancora votato fa redirect a televoto, altriementi apsetta
    fare timeout in js che ogni x secondi controlla se è presente una nuova domanda.
    aggiungere bottone torna ad home page.
  */

  //Prendo la domanda attiva
  $domanda_attiva     = db_query_generale('televoto', ' attiva = 1 ', 'id');
  $res_domanda_attiva = mysql_fetch_assoc($domanda_attiva);
  $risposte_non_date  = db_query_count('risposte_televoto', ' id_utente = '.$_SESSION['id_utente'].' AND id_domanda = '.$res_domanda_attiva['ID']);
  if ($risposte_non_date == 0) {
    echo "
      <script> window.location.href = 'televoto.php'; </script>
    ";
  }
?>

  <div id="content" class="snap-content">
    <div class="content">
      <div class="clear2"></div>
      <div class="preloader-logo"></div>
      <p class="center-text smaller-text">
        Attesa Domanda...
      </p>
    </div>
    <a class="button button-red button-round right-button" href="index-loggato.php">homepage</a>
  </div>

  <script>
    setTimeout(function(){
      window.location.reload();
    }, 5000);
  </script>
</body>
