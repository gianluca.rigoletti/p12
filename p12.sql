-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Creato il: Mag 29, 2016 alle 09:46
-- Versione del server: 5.5.49
-- Versione PHP: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `p12`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `amministratori`
--

CREATE TABLE `amministratori` (
  `ID` int(10) UNSIGNED NOT NULL,
  `User` varchar(45) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Pword` varchar(45) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Livello` int(10) UNSIGNED DEFAULT NULL COMMENT '1=superadmin; 2=adminrwanda; 3=adminsito',
  `LastLogin` datetime DEFAULT NULL,
  `IPLastLogin` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Mail` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Tel` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Nome` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Cognome` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Cod` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IDCod` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IDSito` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `amministratori`
--

INSERT INTO `amministratori` (`ID`, `User`, `Pword`, `Livello`, `LastLogin`, `IPLastLogin`, `Mail`, `Tel`, `Nome`, `Cognome`, `Cod`, `IDCod`, `IDSito`) VALUES
(17, 'd', 'd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `gradimento`
--

CREATE TABLE `gradimento` (
  `id_utente` int(11) NOT NULL,
  `codice_sessione` varchar(8) NOT NULL,
  `valutazione` int(11) DEFAULT NULL,
  `commento` text,
  `nome_relatore` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `gradimento`
--

INSERT INTO `gradimento` (`id_utente`, `codice_sessione`, `valutazione`, `commento`, `nome_relatore`) VALUES
(1, '17_10-45', 3, 'Ottima Rispota', 'Nicola Meotti, Director - Financial Institutions - AB');

-- --------------------------------------------------------

--
-- Struttura della tabella `questionario`
--

CREATE TABLE `questionario` (
  `id_utente` int(11) NOT NULL,
  `valori1` varchar(200) DEFAULT NULL,
  `descrizione1` text,
  `domanda1_altro` text,
  `domanda2` text,
  `domanda3` text,
  `valori4` varchar(200) DEFAULT NULL,
  `descrizione4` text,
  `valori5` varchar(200) DEFAULT NULL,
  `descrizione5` text,
  `domanda6` int(11) DEFAULT NULL,
  `valori7` varchar(200) DEFAULT NULL,
  `descrizione7` text,
  `domanda8` text,
  `domanda9` text,
  `domanda10` int(11) DEFAULT NULL,
  `domanda11` text,
  `domanda11_commento` text,
  `domanda12` text,
  `domanda13` int(11) DEFAULT NULL,
  `valori14` varchar(200) DEFAULT NULL,
  `descrizione14` text,
  `domanda15` text,
  `domanda16` text,
  `domanda17` int(11) DEFAULT NULL,
  `domanda18` int(11) DEFAULT NULL,
  `domanda19` text,
  `domanda20` text,
  `domanda21` int(11) DEFAULT NULL,
  `domanda22` int(11) DEFAULT NULL,
  `domanda23` int(11) DEFAULT NULL,
  `domanda24` int(11) DEFAULT NULL,
  `domanda25` text,
  `domanda26` text,
  `domanda27` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `questionario`
--

INSERT INTO `questionario` (`id_utente`, `valori1`, `descrizione1`, `domanda1_altro`, `domanda2`, `domanda3`, `valori4`, `descrizione4`, `valori5`, `descrizione5`, `domanda6`, `valori7`, `descrizione7`, `domanda8`, `domanda9`, `domanda10`, `domanda11`, `domanda11_commento`, `domanda12`, `domanda13`, `valori14`, `descrizione14`, `domanda15`, `domanda16`, `domanda17`, `domanda18`, `domanda19`, `domanda20`, `domanda21`, `domanda22`, `domanda23`, `domanda24`, `domanda25`, `domanda26`, `domanda27`) VALUES
(1, '2', 'Cassa di previdenza', '', 'Ciao ciaodsds', 'dasdas', '2;5', 'Atene 2006;Berlino 2009', '1;3', 'Il tema del convegno;Poter discutere temi d’investimento con i rappresentanti dell’industria', 6, '2;5', 'xxx – Cassa xxx;xxx – xxx', 'dsdas', 'ddadas', 6, '2', '', '', 0, '', '', '', '', 0, 0, '', '', 0, 0, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `risposte_televoto`
--

CREATE TABLE `risposte_televoto` (
  `ID` int(20) NOT NULL,
  `id_utente` int(20) NOT NULL,
  `id_domanda` int(20) NOT NULL,
  `risposta` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `risposte_televoto`
--

INSERT INTO `risposte_televoto` (`ID`, `id_utente`, `id_domanda`, `risposta`) VALUES
(4, 1, 2, 2),
(5, 1, 3, 1),
(8, 1, 3, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `televoto`
--

CREATE TABLE `televoto` (
  `ID` int(20) NOT NULL,
  `ordine` int(10) NOT NULL,
  `domanda` varchar(500) NOT NULL,
  `risposta_1` varchar(500) NOT NULL,
  `risposta_2` varchar(500) NOT NULL,
  `risposta_3` varchar(500) NOT NULL,
  `risposta_4` varchar(500) NOT NULL,
  `risposta_esatta` int(1) NOT NULL,
  `attiva` int(1) NOT NULL,
  `risposta_1_a` int(6) DEFAULT NULL,
  `risposta_1_b` int(6) DEFAULT NULL,
  `risposta_1_c` int(6) DEFAULT NULL,
  `risposta_2_a` int(6) DEFAULT NULL,
  `risposta_2_b` int(6) DEFAULT NULL,
  `risposta_2_c` int(6) DEFAULT NULL,
  `risposta_3_a` int(6) DEFAULT NULL,
  `risposta_3_b` int(6) DEFAULT NULL,
  `risposta_3_c` int(6) DEFAULT NULL,
  `risposta_4_a` int(6) DEFAULT NULL,
  `risposta_4_b` int(6) DEFAULT NULL,
  `risposta_4_c` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `televoto`
--

INSERT INTO `televoto` (`ID`, `ordine`, `domanda`, `risposta_1`, `risposta_2`, `risposta_3`, `risposta_4`, `risposta_esatta`, `attiva`, `risposta_1_a`, `risposta_1_b`, `risposta_1_c`, `risposta_2_a`, `risposta_2_b`, `risposta_2_c`, `risposta_3_a`, `risposta_3_b`, `risposta_3_c`, `risposta_4_a`, `risposta_4_b`, `risposta_4_c`) VALUES
(2, 2, 'Questa è la seconda domanda. Quale seconda risposta vuoi dare?', 'Ok, grazie', 'Forse, ma non credo', 'Decisamente', 'Assolutamente no', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 'Ecco la terza domanda. ', 'Rispondo 1', 'Rispondo 2', 'Rispondo 3', 'Rispondo 4 ', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 'Ciao', 'Ok', 'No', 'Forseq', 'Potrebbe', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 4, 'cd', 'cd', 'cdcscs', 'ddqdq', 'dqdqwd', 1, 0, 12, 32, 1244, 321, 1231, 123, 12312, 12212, 27, 1231, 12312, 12312);

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `ID` int(20) NOT NULL,
  `utente` int(20) NOT NULL,
  `password` int(20) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `categoria` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`ID`, `utente`, `password`, `nome`, `categoria`) VALUES
(1, 1, 1, 'Gianluca Rigoletti', 2);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `amministratori`
--
ALTER TABLE `amministratori`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `gradimento`
--
ALTER TABLE `gradimento`
  ADD PRIMARY KEY (`id_utente`,`codice_sessione`);

--
-- Indici per le tabelle `risposte_televoto`
--
ALTER TABLE `risposte_televoto`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `televoto`
--
ALTER TABLE `televoto`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ordine` (`ordine`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `amministratori`
--
ALTER TABLE `amministratori`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT per la tabella `risposte_televoto`
--
ALTER TABLE `risposte_televoto`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT per la tabella `televoto`
--
ALTER TABLE `televoto`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
