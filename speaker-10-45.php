<?php
  $title = "Nicola Mattei";
  $no_header = false;
  include "header.php";
?>
<div id="content" class="snap-content">
            <div class="content">
                            <div class="clear2"></div>
                <div class="one-half-responsive">
                <a href="index-loggato.php">
                <img src="immagini/logo-12-in.jpg" width="200" />
                </a>
                </div>
                <div class="one-half-responsive last-column">
                <img class="logo2" src="immagini/logo-12.jpg" width="200" />
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
                <div class="clear"></div>

                <div class="two-third-responsive blue">
                <h1>relatori</h1>
                </div>

                <div class="one-third-responsive last-column red2">
                </div>


                <div class="clear"></div>

                <div class="one-third-responsive">
                <img class="responsive-image half-bottom" src="immagini/relatori/prova.jpg" />

                </div>

                <div class="two-third-responsive supervisor last-column">
                <img class="logo3" src="immagini/relatori/loghi/prova.jpg" />
                <h1>Nicola Mattei</h1>
                <em>Director - Financial Institutions - AB</em>

                <p>Nicola lavora presso AllianceBernstein in Italia dal 2005 e si occupa dello sviluppo dei canali distributivi ed il supporto agli investitori istituzionali. Dal 1998 al 2004 ha ricoperto incarichi analoghi per conto di Gartmore Investment Management e ING Investment Management. Ha inoltre maturato in passato esperienze nel settore del prestito titoli e delle asset securitization in Deutsche Bank, JPMorgan e Citibank. Si è laureato nel 1989 in Economia e Commercio presso
l'Università Bocconi e ha frequentato nel 1999 l'Executive Development Program presso la SDA Bocconi</p>

                </div>



                <div class="clear"></div>


            </div>
             <?php include "footer.php" ?>

        </div>



</body>
